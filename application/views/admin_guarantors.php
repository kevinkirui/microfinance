<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All Guarantors</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Name</th>
                  
                  
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                  <th>Loan Officer</th>
                  <th>Loan Officer Number</th>
                  <th>Occupation</th>
                  
                  
                  <th>Creation Date</th>
                  <th>Edit</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                
                 <td><?php echo $row->guarantor_fname.' '.$row->guarantor_lname; ?></td>
                
                 
                 <td class="center"><?php echo $row->guarantor_phone;?></td>
                  <td class="center"><?php echo $row->guarantor_id_number;?></td>
                 <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->officer_phone;?></td>
               
                  <td class="center"><?php echo $row->guarantor_occupation;?></td>
                  
                   
                 
                 


                  <td class="center"><?php 
                 // echo $row->added_date;
                   echo date("d-m-Y H:i:s", strtotime($row->added_date));
                  ?></td>
                  <td class="center"> 
                    <a style="color:red" Onclick="return confirm('Are you sure you want to edit this record?')" href="<?php  echo site_url("admin/edit_guarantor/$row->guarantor_id"); ?>">Edit</a>

                  </td>
                  
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>