<!DOCTYPE html>
<html lang="en">
<head>
<title>Rafric Microfinance</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/fullcalendar.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-media.css'); ?>" />
<link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url('css/jquery.gritter.css'); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php  echo base_url('js/jquery.min.js'); ?>"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  
  <script>
      $( function() {
        $( "#from" ).datepicker();
         $("#to" ).datepicker();
      } );
  </script>


  
 

</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="#">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome <?php if(isset($jina)){echo $jina;} ?></span><b class="caret"></b></a>
      
    </li>
    <li class="<?php if($this->uri->segment(2)=="dashboard" or $this->uri->segment(2)=="user_login_process" ){echo "active";}?>"><a href="<?php echo base_url('admin/dashboard')?>"><i class="icon icon-home"></i> <span>DASHBOARD</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="approve" or $this->uri->segment(2)=="approve" ){echo "active";}?>"><a href="<?php echo base_url('admin/approve')?>"><i class="icon icon-home"></i> <span class="label label-important"><?php if(isset($requestcount)){ echo $requestcount; } ?></span><span>LOAN REQUESTS</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="disburse" or $this->uri->segment(2)=="disburse" ){echo "active";}?>"><a href="<?php echo base_url('admin/disburse')?>"><i class="icon icon-home"></i><span class="label label-important"><?php if(isset($disbursecount)){ echo $disbursecount; } ?> </span><span>DISBURSE LOANS</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="sms_list" or $this->uri->segment(2)=="sms_list" ){echo "active";}?>"><a href="<?php echo base_url('admin/sms_list')?>"><i class="icon icon-inbox"></i><span>SMS</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="activeloans" or $this->uri->segment(2)=="all_customers" ){echo "active";}?>"><a href="<?php echo base_url('admin/all_customers')?>"><i class="icon icon-home"></i> <span>ALL CUSTOMERS</span></a> </li>
     <li class="<?php if($this->uri->segment(2)=="all_guarantors" or $this->uri->segment(2)=="all_guarantors" ){echo "active";}?>"> <a href="<?php echo base_url('admin/all_guarantors')?>"  ><i class="icon icon-th-list"></i> <span>ALL GUARANTORS</span></a></li>

    <li class=""><a title="" href="<?php echo base_url('admin/change')?>"><i class="icon icon-cog"></i> <span class="text">Change Password</span></a></li>
    <li class=""><a  href="<?php echo base_url('admin/logout')?>"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>

<?php include 'sidebar.php'; ?>
<!-- CONTENT AREA -->

