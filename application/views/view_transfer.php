<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-warning">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
              if(isset($messo))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $messo;
                ?>
               </div>
            <?php
              }
                  
            
            ?>
            <?php echo validation_errors(); ?>
            <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Funds tranasfer to registration fees</h5>
        </div>
        <div class="widget-content nopadding">
          <form  method="post"  action="<?php echo base_url('admin/transfer_process')?>" class="form-horizontal">
       
            <div class="control-group">
              <label class="control-label">Phone :</label>
              <div class="controls">
                <input required type="text" name="phone" class="span9" placeholder=""  value="<?php echo $row->received_phone; ?>" readonly/>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Last Paid amount :</label>
              <div class="controls">
                <input required type="text" name="last_paid" class="span9" placeholder="" value="<?php echo $row->received_amount; ?>" readonly />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Transfer amount</label>
              <div class="controls">
                <input required type="text" name="amount" class="span9" placeholder="Amount to be transferred" />
              </div>
            </div>
             <?php
                }
             ?>
            
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Transfer</button>
            </div>
          </form>
        
       
    </div>
  </div>
</div></div>
<!--Footer-part-->
<?php include 'footer.php'; ?>