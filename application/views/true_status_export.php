 <?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=loanstatus.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
 <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Name</th>
                  <th>Phone</th>
                  <th>National ID</th>
                  
                  <th>Occupation</th>
                  <th>Loan Ref NO</th>
                  <th>Disbursed Amount</th>
                  <th>Due Amount</th>
                  <th>Product Name</th>
                  <th>Start Date</th>
                  <th>Next Due Date</th>
                 
                  
                  <th>Status</th>
                  
                  <th>Received Total</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                   <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                   <td class="center kod"><?php echo $row->ref_no;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>
                  <td class="center"><?php 
                                             $that= $row->mpesa_amount*1.2;
                                           $amount = number_format($that, 2, '.', '');
                                           echo $amount;
                                           
                                           ?></td>
                   <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center"><?php
                  
                   
                  
                  $newDate = date("d-m-Y H:i:s", strtotime($row->mpesa_date));
                  echo $newDate ;
                  ?></td>
                   <td class="center">
                       <?php
                       if(isset($leo))
                       {
                           $de=$leo;
                       }
                      // print_r($de);
                      // $de='2017-01-28 08:35:22';
                       $today=date('Y-m-d ', strtotime($de));
                       
                       //compare expected and received
                       date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($de);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //echo $installmentnumber;
                       // echo "\t";
                        $flo=floor($installmentnumber);
                        
                       // echo $flo;
                        
                        //get installment amount
                        $inst=$row->mpesa_amount/$row->weeks;
                        $am=$inst*1.2;
                        //echo $am;
                        
                        
                        
                        $expe=$flo*$am;
                        //echo $expe;
                        
                        $received=$row->total;
                        
                        
                       
                       
                       
                       
                       
                       
                       
                       
                      
                       if($row->product_id==1)
                       {
                       
                        date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                      if($received>=$expe)
                      {  
                          //get total amount
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fourthweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                        if($firstweek>=$today )
                        {
                            
                             echo date("d-m-Y H:i:s", strtotime($firstweek));
                            
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                           
                             echo date("d-m-Y H:i:s", strtotime($secondweek));
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($thirdweek));

                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                         elseif($today>=$fourthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                      }
                        
                      }
                       elseif($row->product_id==2)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                       
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                        if($received>=$expe)
                      {  
                          //get total amount
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4 AND  $wiki<5)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fifthweek));

                           }
                            elseif($wiki>=5 AND  $wiki<6)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                            elseif($wiki>=6 )
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                        if($firstweek>=$today)
                        {
                             echo date("d-m-Y H:i:s", strtotime($firstweek));
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                             echo date("d-m-Y H:i:s", strtotime($secondweek));

                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($thirdweek));
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fourthweek));

                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fifthweek));
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                        
                         elseif($today>=$sixthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                           
                       }
                       }
                        elseif($row->product_id==3)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                        
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $seventhwee=  date_add($startdate, date_interval_create_from_date_string('49 days'));
                          $seventhweek =    date_format($seventhwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $eightwee=  date_add($startdate, date_interval_create_from_date_string('56 days'));
                          $eightweek =    date_format($eightwee, 'Y-m-d H:i:s');
                          
                                  if($received>=$expe)
                      {  
                          //get total amount
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4 AND  $wiki<5)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fifthweek));

                           }
                            elseif($wiki>=5 AND  $wiki<6)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                            elseif($wiki>=6 AND  $wiki<7)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($seventhweek));

                           }
                           
                           elseif($wiki>=7 AND  $wiki<8)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($eightweek));

                           }
                           
                            elseif($wiki>=8)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($eightweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                          
                        if($firstweek>=$today)
                        {
                          echo date("d-m-Y H:i:s", strtotime($firstweek));

                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            echo date("d-m-Y H:i:s", strtotime($secondweek));
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($thirdweek));
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fifthweek));
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                        
                        
                        elseif($seventhweek>$today AND $today>$sixthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($seventhweek));
                        }
                        
                        elseif($eightweek>$today AND $today>$seventhweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($eightweek));
                        }
                        
                         elseif($today>=$eightweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($eightweek));
                        }
                      }   
                       }
                        ?>
                   </td>
                 
                  
                  
                 
                  <td>
                      <?php
                        date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($de);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //echo $installmentnumber;
                       // echo "\t";
                        $flo=floor($installmentnumber);
                        
                       // echo $flo;
                        
                        //get installment amount
                        $inst=$row->mpesa_amount/$row->weeks;
                        $am=$inst*1.2;
                        //echo $am;
                        
                        
                        
                        $expe=$flo*$am;
                        //echo $expe;
                        
                        $received=$row->total;
                        if($received>=$expe)
                        {
                            echo "active";
                        }
                        else
                        {
                             echo "overdue";
  
                        }
                         
                        

                        
                      
                      ?>
                  </td>
                  
                  <td>
                      <?php
                         if($row->total)
                         {
                                  echo $row->total;
                                  
                         }
                         else
                         {
                             echo '0';
                         }
                       ?>
                  </td>
               

                  
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>