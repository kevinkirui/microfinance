<?php include 'officer_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
                  
             
            $this->load->library('form_validation');
            
            echo validation_errors(); 
            
            
            ?>  
            
           
      
            
    
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Client & Guarantor Details</h5>
       
       </div>
        <div class="widget-content nopadding">
         <form  id="form-wizard"  method="post" action="<?php echo base_url('officer/add_client_process')?>"  class="form-horizontal" enctype="multipart/form-data">
          <div id="form-wizard-1" class="step"> 
            <div class="control-group">
              <label class="control-label">Client First Name :</label>
              <div class="controls">
                <input type="text" name="customer_fname" value="" required class="span9" placeholder="First name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Middle Name :</label>
              <div class="controls">
                <input type="text" name="customer_mname" value=""  class="span9" placeholder="Middle name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Last Name :</label>
              <div class="controls">
                <input type="text" name="customer_lname" value="<?=set_value('customer_lname')?>" required class="span9" placeholder="Last name" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Client National ID NO. :</label>
              <div class="controls">
                <input type="number" name="customer_id_number" value="<?=set_value('customer_id_number')?>" required class="span9" placeholder="National ID" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Phone Number :</label>
              <div class="controls">
                <input type="number" name="customer_phone" value="<?=set_value('customer_phone')?>"  required class="span9" placeholder="Phone Number" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Email(Optional)</label>
              <div class="controls">
                <input type="email" name="customer_email" value="<?=set_value('customer_email')?>" class="span9" placeholder="Email" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Occupation</label>
              <div class="controls">
                <input type="text" name="customer_occupation" value="<?=set_value('customer_occupation')?>" required class="span9" placeholder="Occupation" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Document (optional)</label>
              <div class="controls">
                <input type="file" name="userfile"   class="span9"  />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Client Location</label>
              <div class="controls">
                    <select name="location" required>
                      <option  value=""></option>
                <?php
                                 foreach ($level->result_array() as $row)
 
                                    { ?>
              
                  <option  value="<?php echo $row['location_id']; ?>"><?php echo $row['location_name']; ?></option>
                                 <?php
                                    }
                                 ?>
                </select>
              </div>
            </div>
           </div>
          
            <!-- guarantor starts here  --> 
        <div id="form-wizard-2" class="step">
             <div class="control-group">
              <label class="control-label">Guarantor First Name :</label>
              <div class="controls">
                <input type="text" name="guarantor_fname" value="<?=set_value('guarantor_fname')?>" required class="span9" placeholder="First name" />
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Guarantor Middle Name :</label>
              <div class="controls">
                <input type="text" name="guarantor_mname" value="<?=set_value('guarantor_mname')?>"  class="span9" placeholder="Middle name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Guarantor Last Name :</label>
              <div class="controls">
                <input type="text" name="guarantor_lname" value="<?=set_value('guarantor_lname')?>" required class="span9" placeholder="Last name" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Guarantor National ID NO. :</label>
              <div class="controls">
                <input type="number" name="guarantor_id_number" value="<?=set_value('guarantor_id_number')?>" required class="span9" placeholder="National ID" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Guarantor Phone Number :</label>
              <div class="controls">
                <input type="number" name="guarantor_phone" value="<?=set_value('guarantor_phone')?>"  required class="span9" placeholder="Phone Number" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Guarantor KRAPIN(optional) :</label>
              <div class="controls">
                <input type="text" name="guarantor_krapin" value="<?=set_value('guarantor_krapin')?>"   class="span9" placeholder="KRA pin" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Guarantor Email(Optional)</label>
              <div class="controls">
                <input type="email" name="guarantor_email" value="<?=set_value('guarantor_email')?>" class="span9" placeholder="Email" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Guarantor Occupation</label>
              <div class="controls">
                <input type="text" name="guarantor_occupation" value="<?=set_value('guarantor_occupation')?>" required class="span9" placeholder="Occupation" />
              </div>
            </div>
            
          </div>  
           
            
            <div class="form-actions">
               <input id="back" class="btn btn-primary" type="reset" value="Back" />
              <input type="submit" class="btn btn-success" value="Next" />
              <div id="status"></div>
            </div>
            <div id="submitted"></div>
          </form>
        
       
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
               

<!--Footer-part-->
<?php include 'footer.php';  ?>