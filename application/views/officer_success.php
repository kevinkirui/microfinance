<?php include 'officer_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
       
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Client added successfully</h5>
        </div>
        <div class="widget-content nopadding">
            <div class="alert alert-success" style="font-size:18px">
             <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
              
            <?php
              }
                  
            
            ?>
            </div>
          <div class="form-actions">
              <a href="<?php echo base_url('officer/add_client'); ?>">
              <input type="submit" class="btn btn-success" value="Go Back" />
              </a>
              <div id="status"></div>
            </div>
        
       </div>
    </div>
  </div>
</div></div>
<!--Footer-part-->
<?php include 'footer.php'; ?>