<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
    <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        
    </div>
     <div class="span3">
           
     </div>
      <div class="span2">
         
     </div>
      </form>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           if(isset($phone))
           {
               $phone=$phone;
           }
         
         ?>
        
         <a href="<?php echo base_url('Admin/view_bankstatement_export/'.$phone); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
      
    </div>
  </div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Mpesa Statement</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  
                 
                  <th>Debit</th>
                  <th>Credit</th>
                  
                  <th>Mpesa Code</th>
                  <th>Transaction Date</th>
                  
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                $disbursed=0;
                $count=0;
                if(isset($h)){
                foreach ($h->result() as $row)  
                {  
                 $count++;
                 if($row->mpesa_type=='D'){
                 ?>
                    <tr class="odd gradeX">
                     
                    
                       <td>_</td>
                        <td><?php 
                     
                         $disbursed= -$row->pesa;
                         echo number_format($disbursed, 2, '.', '');
                         //echo $disbursed;
                      
                       ?></td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center"><?php
                
                 echo date("d-m-Y H:i:s", strtotime($row->dato));
                 ?>
                 </td>


                  </tr> 
                     

                 
                 <?php 
                  }
                  

                     

                 
                
                  
                
                  
                  else
                  {
                      
                      ?>
                   <tr class="odd gradeX">
                             
                     
                     <td><?php 
                     
                         $received=$row->pesa;
                         echo $received;
                      
                       ?></td>
                       <td>_</td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center">
                     <?php
                                      echo date("d-m-Y H:i:s", strtotime($row->dato));

                     
                     ?></td>
                    
                 </tr> 
                   
               <?php 
                 }
                }
                }
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part--><?php include 'footer.php'; ?>
