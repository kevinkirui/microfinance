<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
 
             <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success" style="font-size:18px">
                <?php
                  echo $message;
                ?>
              </div>
            <?php
              }
                  
            
            ?>
        
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Loan Officers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Clients</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                  <td><?php echo $row->officer_id;?></td>
                  <td><?php echo $row->officer_fname;?></td>
                  <td class="center"> <?php echo $row->officer_lname;?></td>
                  <td class="center"><?php echo $row->officer_nationalid;?></td>
                  <td class="center"><?php echo $row->officer_phone;?></td>
                  <td class="center"><?php echo $row->officer_email;?></td>
                  <?php
                  //encode emails
                  
                    $str=base64_encode($row->officer_email);
                  
                  ?>
                  <td class="center"><a href="  <?php  echo site_url("admin/clients/$str"); ?>"><?php echo $row->count;?></a></td>
                   <td class="center">
                         <a style="color:red" Onclick="return confirm('Are you sure you want to delete this loan officer?')" href="<?php  echo site_url("admin/deleteloanofficer/$row->officer_phone"); ?>">Delete Officer</a></td>
                    </td>
                </tr>
                 <?php }  
                  ?>  
               
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>