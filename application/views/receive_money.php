<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
                  
            
            ?>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Receive Money</h5>
        </div>
       
        <div class="widget-content nopadding">
          <form  method="post"  action="<?php echo base_url('admin/receive_money_process')?>" class="form-horizontal">
       
            <div class="control-group">
              <label class="control-label">Phone Number :</label>
              <div class="controls">
                <input required type="number" name="customer_phone" class="span9" placeholder="Phone Number" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Amount</label>
              <div class="controls">
                <input required type="number" name="amount" class="span9" placeholder="Amount" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Mpesa Code</label>
              <div class="controls">
                <input required type="text" name="mpesa_code" class="span9" placeholder="Mpesa Code" />
              </div>
            </div>
            
            
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Receive</button>
            </div>
          </form>
        
       
    </div>
  </div>
</div></div>
<!--Footer-part-->
<?php include 'footer.php'; ?>