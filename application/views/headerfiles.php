<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/fullcalendar.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-media.css'); ?>" />
<link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url('css/jquery.gritter.css'); ?>" />