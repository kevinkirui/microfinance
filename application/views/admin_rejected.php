<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Rejected Loans</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>Customer Name</th>
                  <th>National ID</th>
                  <th>Customer Phone</th>
                  <th>Email</th>
                  <th>Occupation</th>
                  <th>Loan Officer</th>
                  <th>Amount</th>
                  <th>Loan Ref No</th>
                  <th>Reason</th>
                  <th>Date Rejected</th>

                  
                </tr>
              </thead>
              <tbody>
               
                <?php  
                //CHECK IF RESULTS ARE EMPTY
              
                if(isset($h))
                {
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_email;?></td>
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->rejected_amount;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center"><?php echo $row->rejected_reason;?></td>
                  <td class="center"><?php   echo date("d-m-Y H:i:s", strtotime($row->rejected_time)); ?></td>
                </tr>
                 <?php } 
                 
                }
                  ?>  
               
              </tbody>
            </table>
               <?php
                     if(isset ($message)) {
                   ?>
              <div class="alert alert-warning" style="text-align:center">
                   <strong><?php echo $message; ?></strong>
              </div>
              <?php     
                  }
                 ?> 
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>