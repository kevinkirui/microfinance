<!DOCTYPE html>
<html lang="en">
<head>
<title>Rafric Microfinance</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/fullcalendar.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>" />

<link rel="stylesheet" href="<?php echo base_url('css/matrix-media.css'); ?>" />
<link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url('css/jquery.gritter.css'); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php  echo base_url('js/jquery.min.js'); ?>"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  
  <script>
      $( function() {
        $( "#from" ).datepicker();
         $("#to" ).datepicker();
      } );
  </script>

</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="#">Officer Login</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome <?php if(isset($jina)){ echo $jina; }?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-check"></i> My Tasks</a></li>
        <li class="divider"></li>
        <li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
      </ul>
    </li>
    
    <li class=""><a title="" href="<?php echo base_url('officer/change_view')?>"><i class="icon icon-cog"></i> <span class="text">Change Password</span></a></li>
    <li class=""><a title="" href="<?php echo base_url('officer/logout')?>"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>

<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="<?php if($this->uri->segment(2)=="my_clients" or $this->uri->segment(2)=="user_login_process" ){echo "active";}?>"><a href="<?php echo base_url('officer/my_clients')?>"><i class="icon icon-home"></i> <span>My Clients</span></a> </li>
   <li class="<?php if($this->uri->segment(2)=="all_customers" or $this->uri->segment(2)=="guarantor_details" ){echo "active";}?>"><a href="<?php echo base_url('officer/all_customers')?>"><i class="icon icon-home"></i> <span>All Clients</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="my_arrears" or $this->uri->segment(2)=="my_arrears" ){echo "active";}?>"><a href="<?php echo base_url('officer/my_arrears')?>"><i class="icon icon-home"></i> <span>My Arrears</span></a> </li>

    <li class="<?php if($this->uri->segment(2)=="loans_duetoday" or $this->uri->segment(2)=="loans_duetoday" ){echo "active";}?>"><a href="<?php echo base_url('officer/loans_duetoday')?>"><i class="icon icon-home"></i> <span>Loans Due Today</span></a> </li>

    <!--
     <li class="<?php if($this->uri->segment(2)=="clients_active"){echo "active";}?>"> <a href="<?php echo base_url('officer/clients_active')?>"><i class="icon icon-signal"></i> <span>Active Loans</span></a> </li> 
    <li class="<?php if($this->uri->segment(2)=="clients_rejected"){echo "active";}?>"> <a href="<?php echo base_url('officer/clients_rejected')?>"><i class="icon icon-inbox"></i> <span>Rejected Loans</span></a> </li>-->

    <li  class="<?php if($this->uri->segment(2)=="add_client"){echo "active";}?>"><a href="<?php echo base_url('officer/add_client')?>"><i class="icon icon-th"></i> <span>Add Client</span></a></li>
    <li class="<?php if($this->uri->segment(2)=="truestatus" or $this->uri->segment(2)=="truestatus" ){echo "active";}?>"><a href="<?php echo base_url('officer/truestatus')?>"><i class="icon icon-home"></i> <span>Loan Status</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="activeloans"){echo "active";}?>"> <a href="<?php echo base_url('officer/activeloans')?>"><i class="icon icon-signal"></i> <span>Active Loans</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="loan_arrears" or $this->uri->segment(2)=="loan_arrears" ){echo "active";}?>"><a href="<?php echo base_url('officer/loan_arrears')?>"><i class="icon icon-pencil"></i> <span>Loan Arrears</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="all_disbursed" or $this->uri->segment(2)=="all_disbursed" ){echo "active";}?>"><a href="<?php echo base_url('officer/all_disbursed')?>"><i class="icon icon-file"></i> <span>Disbursed Loans</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="rejected"){echo "active";}?>"> <a href="<?php echo base_url('officer/rejected')?>"><i class="icon icon-th-list"></i> <span>Rejected Loans</span> <span class="label label-important"></span></a>
    <li class="<?php if($this->uri->segment(2)=="admin_balance" or $this->uri->segment(2)=="admin_balance" ){echo "active";}?>"><a href="<?php echo base_url('officer/admin_balance')?>"><i class="icon icon-tint"></i> <span>Loan Balance</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="mpesa_repayments" or $this->uri->segment(2)=="view_statement" ){echo "active";}?>"><a href="<?php echo base_url('officer/mpesa_repayments')?>"><i class="icon icon-inbox"></i> <span>Mpesa Repayments</span></a> </li>

    <li class="<?php if($this->uri->segment(2)=="all_loans" or $this->uri->segment(2)=="all_loans" ){echo "active";}?>"><a href="<?php echo base_url('officer/all_loans')?>"><i class="icon icon-info-sign"></i> <span>All Loan Requests</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="pending_approval"){echo "active";}?>"> <a href="<?php echo base_url('officer/pending_approval')?>"><i class="icon icon-th-list"></i> <span>Loans Pending Approval</span> <span class="label label-important"></span></a>

    <li class="<?php if($this->uri->segment(2)=="awaiting_disbursement"){echo "active";}?>"> <a href="<?php echo base_url('officer/awaiting_disbursement')?>"><i class="icon icon-th-list"></i> <span>Loans Pending Disbursement</span> <span class="label label-important"></span></a>


   
  </ul>
</div>
<!-- CONTENT AREA -->
