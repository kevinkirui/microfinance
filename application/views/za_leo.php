<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Loans due Today</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Created Date</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>National ID</th>
                  
                  <th>Occupation</th>
                  <th>Disbursed Amount</th>
                  <th>Due Amount</th>
                  <th>Product Name</th>
                  <th>Start Date</th>
                  <th>Next Due Date</th>
                 
                  
                  <th>Status</th>
                  
                  <th>Received Total</th>
                  <th>Payment schedule</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                    foreach ($h->result() as $row)  
                {  
                        $firstdeadline="";
                        $seconddeadline="";
                        $thirddeadline="";
                      $de='2017-02-03 09:55:30';
                       $today=date('Y-m-d ', strtotime($de));
                       if($row->product_id==1)
                       {
                        date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          
                        if($firstweek>=$today)
                        {
                            $firstdeadline =$firstweek;
                          
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                             $firstdeadline = $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                             $firstdeadline = $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                             $firstdeadline = $fourthweek;
                        }
                        
                         elseif($today>=$fourthweek)
                        {
                             $firstdeadline = $fourthweek;
                        }
                       }
                       elseif($row->product_id==2)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                       
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          
                        if($firstweek>=$today)
                        {
                             $seconddeadline = $firstweek;
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            $seconddeadline= $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            $seconddeadline= $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            $seconddeadline=$fourthweek;
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            $seconddeadline= $fifthweek;
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                            $seconddeadline= $sixthweek;
                        }
                        
                         elseif($today>=$sixthweek)
                        {
                             $seconddeadline= $sixthweek;
                        }
                           
                       }
                       
                        elseif($row->product_id==3)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                        
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $seventhwee=  date_add($startdate, date_interval_create_from_date_string('49 days'));
                          $seventhweek =    date_format($seventhwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $eightwee=  date_add($startdate, date_interval_create_from_date_string('56 days'));
                          $eightweek =    date_format($eightwee, 'Y-m-d H:i:s');
                          
                       
                          
                          
                        if($firstweek>=$today)
                        {
                             $thirddeadline= $sixthweek;$firstweek;
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            $thirddeadline= $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            $thirddeadline= $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            $thirddeadline=  $fourthweek;
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            $thirddeadline=  $fifthweek;
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                           $thirddeadline=  $sixthweek;
                        }
                        
                        
                        elseif($seventhweek>$today AND $today>$sixthweek)
                        {
                            $thirddeadline=  $seventhweek;
                        }
                        
                        elseif($eightweek>$today AND $today>$seventhweek)
                        {
                            $thirddeadline= $eightweek;
                        }
                        
                         elseif($today>=$eightweek)
                        {
                             $thirddeadline= $eightweek;
                        }
                           
                       }
                       
                      
                     
                      //$firstdead =   date_format($firstdeadline, 'Y-m-d ');
                      //echo $firstdead;
                  if(($firstdeadline==$de) OR ($seconddeadline==$de) OR ($thirddeadline==$de))
                   {
                        
                foreach ($h->result() as $row)  
                {  
                    
                 ?>
                <tr class="odd gradeX">
                  <td>Date</td>
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                   <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>
                  <td class="center"><?php echo $row->mpesa_amount*1.2;?></td>
                   <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center"><?php echo $row->mpesa_date;?></td>
                   <td class="center">
                       <?php
                       $de='2017-01-28 08:35:22';
                       $today=date('Y-m-d ', strtotime($de));
                       if($row->product_id==1)
                       {
                        date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          
                        if($firstweek>=$today)
                        {
                            echo $firstweek;
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            echo $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            echo $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo $fourthweek;
                        }
                        
                         elseif($today>=$fourthweek)
                        {
                            echo $fourthweek;
                        }
                       }
                       elseif($row->product_id==2)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                       
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          
                        if($firstweek>=$today)
                        {
                            echo $firstweek;
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            echo $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            echo $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo $fourthweek;
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            echo $fifthweek;
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                            echo $sixthweek;
                        }
                        
                         elseif($today>=$sixthweek)
                        {
                            echo $sixthweek;
                        }
                           
                       }
                       
                        elseif($row->product_id==3)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                        
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $seventhwee=  date_add($startdate, date_interval_create_from_date_string('49 days'));
                          $seventhweek =    date_format($seventhwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $eightwee=  date_add($startdate, date_interval_create_from_date_string('56 days'));
                          $eightweek =    date_format($eightwee, 'Y-m-d H:i:s');
                          
                       
                          
                          
                        if($firstweek>=$today)
                        {
                            echo $firstweek;
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            echo $secondweek;
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            echo $thirdweek;
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo $fourthweek;
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            echo $fifthweek;
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                            echo $sixthweek;
                        }
                        
                        
                        elseif($seventhweek>$today AND $today>$sixthweek)
                        {
                            echo $seventhweek;
                        }
                        
                        elseif($eightweek>$today AND $today>$seventhweek)
                        {
                            echo $eightweek;
                        }
                        
                         elseif($today>=$eightweek)
                        {
                            echo $eightweek;
                        }
                           
                       }
                        ?>
                   </td>
                 
                  
                  
                 
                  <td>
                      <?php
                        date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime("2017-01-28 10:35:22");
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //echo $installmentnumber;
                       // echo "\t";
                        $flo=floor($installmentnumber);
                        
                       // echo $flo;
                        
                        //get installment amount
                        $inst=$row->mpesa_amount/$row->weeks;
                        $am=$inst*1.2;
                        //echo $am;
                        
                        
                        
                        $expe=$flo*$am;
                        //echo $expe;
                        
                        $received=$row->total;
                        if($received>=$expe)
                        {
                            echo "active";
                        }
                        else
                        {
                             echo "overdue";
  
                        }
                         
                        

                        
                      
                      ?>
                  </td>
                  
                  <td>
                      <?php
                         if($row->total)
                         {
                                  echo $row->total;
                                  
                         }
                         else
                         {
                             echo '0';
                         }
                       ?>
                  </td>
                 <td><a href="<?php  echo site_url("admin/paymentschedule/$row->customer_phone"); ?>">Payment Schedule</a></td>

                  
                </tr>
                 <?php }  
                   }
                }
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>
