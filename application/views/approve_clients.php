<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Loan Requests</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Created Date</th>
                  <th>Customer Name</th>
                 
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Product</th>
                  <th>Ref No</th>
                   <th>Amount</th>
                   <th>Interest</th>
                  <th>Status</th>
                  <th>Branch</th>
                  <th>Occupation</th>
                  <th>Guarantor</th>
                  <th>Docs</th>
                  <th>Approve</th>
                  <th>Reject</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                if(isset($h))
                {
                    foreach ($h->result() as $row)  
                    {  
                 ?>
                <tr class="odd gradeX">
                  <td><?php echo $row->requested_date;?></td>
                  <td><?php echo $row->customer_fname."\t ".$row->customer_lname ?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center " style="text-transform: uppercase;"><?php echo $row->ref_no;?></td>
                  <td class="center"><?php echo $row->request_amount;?></td>
                  <td class="center"><?php echo $row->request_amount*0.2;?></td>
                  <td class="center"><?php echo $row->status_name;?></td>
                    <td class="center"><?php echo $row->location_name;?></td>
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td><a style="color:blue" href="<?php  echo site_url("admin/guarantor_details/$row->customer_phone"); ?>">Guarantor Details</a></td>
                   <?php
                     if($row->customer_document)
                     {
                  ?>
                  <td><a style="color:blue" href="<?php  echo base_url("uploads/$row->customer_document"); ?>">Document</a></td>
                  <?php
                     }
                  
                 
                  else
                  { ?>
                   <td></td>
                  <?php
                  }
                  ?>

                  <td class="center"><a href="  <?php  echo site_url("admin/approve_process/$row->customer_id_number"); ?>"><button type="button" class="btn btn-success">Approve</button></a></td>
                  <td class="center"><a href="  <?php  echo site_url("admin/reject_reason/$row->customer_id_number"); ?>"><button type="button" class="btn btn-danger">Reject</button></a></td>
                </tr>
                 <?php 
                 
                      }  
                    }
                  ?>  
               
              </tbody>
            </table>
            <?php
                     if(isset ($message)) {
                   ?>
              <div class="alert alert-warning" style="text-align:center">
                   <strong><?php echo $message; ?></strong>
              </div>
              <?php     
                  }
                 ?> 
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>