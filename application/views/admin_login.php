<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Matrix Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('css/matrix-login.css'); ?>" />
        <link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">  
            <?php
                                                                  
                if(isset($error_message))
                {
                    echo '<div class="err" style="color:red; font-size:12px; text-align:center">';
                   echo  $error_message;
                     echo '</div>';
                }
                
                if(isset($message_display))
                {
                    echo '<div class="err" style="color:green; font-size:12px; text-align:center">';
                   echo  $message_display;
                     echo '</div>';
                }

             ?>
            <form id="loginform" class="form-vertical" method="post" action="<?php echo base_url('admin/user_login_process')?>">
				 <div class="control-group normal_text"> <h4>Admin Login</h4></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="email" placeholder="email" name="admin_email"/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="admin_password"/>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
            </form>
           
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
    </body>

</html>
