<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('officer/admin_balance'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input required name="from" type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input required type="text" name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
      </form>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           //capture from and to
           if(isset($from))
           {
               $from=base64_encode($from);
               $from=$from;
           }
           
           if(isset($to))
           {
               $to=base64_encode($to);
               $to=$to;
           }
         
         ?>
         <a href="<?php echo base_url('Admin/admin_balance_export/'.$from.'/'.$to); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
    
      
    </div>
  </div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Loan Balance</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              
                  
                  

                  
               
              <tbody>
               
                <tr class="odd gradeX">
                 <thead>
                  
                
                                

                 <td>
                 <table class="table table-bordered table-striped">
                    <thead>
                <tr>
                 <th>Branch Name</th>
                 <th>Total Balance</th>
                  
                  <th>Customer Name</th>
                  <th>Mobile Number</th>
                  
                  <th>ID number</th>
                  <th>Total Amount</th>
                  <th>Loan Officer</th>
                  <th>Product Name</th>
                  <th>Loan Ref NO</th>
                  <th>Mpesa Ref</th>
                  <th>Balance</th>
                  

                  
                </tr>
              </thead>
              <?php
               $regthreetot=0;
               $kimumtot=0;
               $mailitot=0;
               
               $regthreerecvd=0;
               $kimumurecvd=0;
               $mailirecvd=0;
               
               ?>
               <tr>
                 
                   <td rowspan="<?php 
                   if(isset($regionthree))
                       { 
                        if($regionthree>0)
                        {
                         
                          echo $regionthree; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>REGION 3</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($regionthree))
                       { 
                        if($regionthree>0)
                        {
                         
                          echo $regionthree; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td>
                             <?php
                               if(isset($regtatuz))
                               {
                                   foreach ($regtatuz->result() as $row)  
                                  { 
                                   
                                        $regthreetot=$regthreetot+$row->mpesa_amount*1.2;
                                        $regthreerecvd=$regthreerecvd+$row->receivedtotal;

                                       
                                        $regobal=$regthreetot-$regthreerecvd;
                                       
                                  }
                                   if(!empty($regobal))
                                   {
                                    // echo $regobal;
                                      echo number_format($regobal, 2, '.', '');
                                   }
                               }
                             ?></td>
                       
                        </tr>
                        
                    </table>
                   </td>
                  
               <?php
               foreach ($regtatu->result() as $row)  
                { 
          
              if($row->location_id==3)
              {
                  
              
              ?>
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php
                                            $she=$row->mpesa_amount*1.2;
                                            echo number_format($she, 2, '.', '');
                                           
                                    ?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"> <?php
                               $totamount=$row->mpesa_amount*1.2;
                               $received=0;
                               if($row->received_date>=$row->mpesa_date)
                               {
                                   $received=$row->total;
                               }
                               $bal=$totamount-$received;
                               echo number_format($bal, 2, '.', '');
                               
                               ?></td>

                
                   
                  </tr>
                  <?php } 
                }
                
                ?>
                 <tr>
                 
                   <td rowspan="<?php 
                   if(isset($kimumucount))
                       { 
                        if($kimumucount>0)
                        {
                         
                          echo $kimumucount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>KIMUMU</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($kimumucount))
                       { 
                        if($kimumucount>0)
                        {
                         
                          echo $kimumucount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td><?php
                               if(isset($kimumtols))
                               {
                                   foreach ($kimumtols->result() as $row)  
                                  { 
                                   
                                        $kimumtot=$kimumtot+$row->mpesa_amount*1.2;
                                        $kimumurecvd=$kimumurecvd+$row->receivedtotal;

                                       
                                        $kimumubal=$kimumtot-$kimumurecvd;
                                       
                                  }
                                  
                                  if(!empty($kimumubal))
                                  {
                                   //echo $kimumubal;
                                   echo number_format($kimumubal, 2, '.', '');
                                  }
                               }
                             ?></td>
                       
                        </tr>
                        
                    </table>
                   </td>
                <?php
                 foreach ($kimbal->result() as $row)  
                { 
                  
                    ?> 
                  
                  <?php
              $kimumucount=0;
              if($row->location_id==1)
              {
                 $kimumucount++
                  ?>  
                  
                  
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php
                                             $lo=$row->mpesa_amount*1.2; 
                                             echo number_format($lo, 2, '.', '');
                                     ?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"><?php
                               $totamount=$row->mpesa_amount*1.2;
                               $received=0;
                               if($row->received_date>=$row->mpesa_date)
                               {
                                   $received=$row->total;
                               }
                               $bal=$totamount-$received;
                                echo number_format($bal, 2, '.', '');
                               //echo $bal; 
                               ?></td>

                 
                   
                  </tr>
                  <?php } 
                }
                ?>
                       <tr>
                 
                   <td rowspan="<?php 
                   if(isset($mailicount))
                       { 
                        if($mailicount>0)
                        {
                         
                          echo $mailicount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>MAILI NNE</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($mailicount))
                       { 
                        if($mailicount>0)
                        {
                         
                          echo $mailicount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td>
                            
                              <?php
                               if(isset($mailitols))
                               {
                                   foreach ($mailitols->result() as $row)  
                                  { 
                                   
                                        $mailitot=$mailitot+$row->mpesa_amount*1.2;
                                        $mailirecvd=$mailirecvd+$row->receivedtotal;

                                       
                                        $mylbal=$mailitot-$mailirecvd;
                                       
                                  }
                                  if(!empty($mylbal))
                                  {
                                    //echo $mylbal;
                                    echo number_format($mylbal, 2, '.', '');
                                  }
                               }
                             ?>
                           
                         </td>
                       
                        </tr>
                        
                    </table>
                   </td>
                 <?php
                 foreach ($mailbal->result() as $row)  
                {  
                
               
                 if($row->location_id==2)
                 {
                 
              
                
                 ?>
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php
                                            $hass=$row->mpesa_amount*1.2; 
                                            echo number_format($hass, 2, '.', '');
                                            
                                            ?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"><?php
                               $totamount=$row->mpesa_amount*1.2;
                               $received=0;
                               if($row->received_date>=$row->mpesa_date)
                               {
                                   $received=$row->total;
                               }
                               $bal=$totamount-$received;
                               echo number_format($bal, 2, '.', '');
                               
                               //echo $bal; 
                               
                               ?></td>

                 
                   
                  </tr>
                  <?php } 
              }
                 
                  ?>  
                 
                   
                   
                  </table> 
                 </td>
                </tr>
                
                <!-- new row-->
                
                
                
                
                
              </tbody>
            </table>
               <table class="table table-bordered table-striped">
                    <tr><td><b>TOTAL</b></td>
                    <td><b><?php
                              
                              if(!empty($regobal))
                              {
                                 $regobal=$regobal;
                              }
                              else
                              {
                                  $regobal=0;
                              }
                              
                              if(!empty($mylbal))
                              {
                                 $mylbal=$mylbal; 
                              }
                              else
                              {
                                  $mylbal=0;
                              }
                              
                              if(!empty($kimumubal))
                              {
                                 $kimumubal=$kimumubal ;
                              }
                              else
                              {
                                  $kimumubal=0;
                              }
                                 $toshtol= $regobal+$mylbal+$kimumubal; 
                                 echo number_format($toshtol, 2, '.', '');
                                 
                    
                    ?></b></td></tr>
                </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>