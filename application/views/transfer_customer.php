<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
                  
             
            $this->load->library('form_validation');
            
            echo validation_errors(); 
            
            
            ?>  
            
           
      
             <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
    
        <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Transfer Client to another loan officer</h5>
       
       </div>
        <div class="widget-content nopadding">
         <form  id="form-wizard"  method="post" action="<?php echo base_url('admin/transfer_customer_process')?>"  class="form-horizontal" enctype="multipart/form-data">
          <div id="form-wizard-1" class="step"> 
            <div class="control-group">
              <label class="control-label">Client First Name :</label>
              <div class="controls">
                <input type="hidden" name="customer_id"  value="<?php echo $row->customer_id; ?>"/>
                <input type="text" name="customer_fname" value="<?php echo $row->customer_fname; ?>" required class="span9" placeholder="First name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Middle Name :</label>
              <div class="controls">
                <input type="text" name="customer_mname" value="<?php echo $row->customer_middlename; ?>"  class="span9" placeholder="Middle name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Client Last Name :</label>
              <div class="controls">
                <input type="text" name="customer_lname" value="<?php echo $row->customer_lname; ?>" required class="span9" placeholder="Last name" />
              </div>
            </div>
              <div class="control-group">
              <label class="control-label">Assign Loan Officer</label>
              <div class="controls">
                    <select name="officer_email" required>
                      <option  value=""></option>
                <?php
                                 foreach ($l->result_array() as $row)
 
                                    { ?>
              
                  <option  value="<?php echo $row['officer_email']; ?>"><?php echo $row['officer_fname'].''.$row['officer_lname']; ?></option>
                                 <?php
                                    }
                                 ?>
                </select>
              </div>
            </div>
             
             
           </div>
            <div class="form-actions">
              
              <input type="submit" class="btn btn-success" value="Edit" />
              <div id="status"></div>
            </div>
           
          </form>
         <?php } ?>
       
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
               

<!--Footer-part-->
<?php include 'footer.php';  ?>