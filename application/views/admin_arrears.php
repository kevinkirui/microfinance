<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
     <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('admin/loan_arrears'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input required name="from" type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input required type="text" name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
      </form>
     <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           //capture from and to
           if(isset($from))
           {
               $from=base64_encode($from);
               $from=$from;
           }
           
           if(isset($to))
           {
               $to=base64_encode($to);
               $to=$to;
           }
         
         ?>
         <a href="<?php echo base_url('Admin/loan_arrears_export/'.$from.'/'.$to); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
      
    </div>
  </div>
  <div class="container-fluid">
   
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5> Loan Arrears Per Loan Officer</h5>
          </div>
           <?php  
                //get arrears for specific officer 
                $tot=0;
                foreach ($h->result() as $row)  
                {  
                        if(isset($leo))
                       {
                           $de=$leo;
                           
                          
                          
                       }
                       
                       
                       $today=date('Y-m-d ', strtotime($de));
                    date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($today);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //if installment is greater than product length arrears is the same as balance
                         $received=$row->total;
                        if($installmentnumber>=$row->weeks)
                        {
                            $expe= $row->mpesa_amount*1.2;
                            $arrears=$expe-$received;
                        }
                        else
                        {
                                 echo "\t";
                            $flo=floor($installmentnumber);
                            
                           // echo $flo;
                            
                            //get installment amount
                            $inst=$row->mpesa_amount/$row->weeks;
                            $am=$inst*1.2;
                            //echo $am;
                            
                            
                            
                            $expe=$flo*$am;
                           
                            
                           
                            $arrears=$expe-$received;
                            
                        }
                        
                        //echo $installmentnumber;
                       
                       
                        
                       
                        if(($expe>$received AND $expe!=0))
                        {
                            $tot=$tot+$arrears;
                        }
                    
                   }    
                     //  echo $tot;  
                     // adila totals starts here
                  $adilatot=0;
                foreach ($adila->result() as $row)  
                {  
                        if(isset($leo))
                       {
                           $de=$leo;
                           
                          
                          
                       }
                       
                       
                       $today=date('Y-m-d ', strtotime($de));
                    date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($today);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //if installment is greater than product length arrears is the same as balance
                         $received=$row->total;
                        if($installmentnumber>=$row->weeks)
                        {
                            $expe= $row->mpesa_amount*1.2;
                            $arrears=$expe-$received;
                        }
                        else
                        {
                                 echo "\t";
                            $flo=floor($installmentnumber);
                            
                           // echo $flo;
                            
                            //get installment amount
                            $inst=$row->mpesa_amount/$row->weeks;
                            $am=$inst*1.2;
                            //echo $am;
                            
                            
                            
                            $expe=$flo*$am;
                           
                            
                           
                            $arrears=$expe-$received;
                            
                        }
                        
                        //echo $installmentnumber;
                       
                       
                        
                       
                        if(($expe>$received AND $expe!=0))
                        {
                            $adilatot=$adilatot+$arrears;
                        }
                    
                   }    
                   //abu totals
                      $abutot=0;
                foreach ($abu->result() as $row)  
                {  
                        if(isset($leo))
                       {
                           $de=$leo;
                           
                          
                          
                       }
                       
                       
                       $today=date('Y-m-d ', strtotime($de));
                    date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($today);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //if installment is greater than product length arrears is the same as balance
                         $received=$row->total;
                        if($installmentnumber>=$row->weeks)
                        {
                            $expe= $row->mpesa_amount*1.2;
                            $arrears=$expe-$received;
                        }
                        else
                        {
                                 echo "\t";
                            $flo=floor($installmentnumber);
                            
                           // echo $flo;
                            
                            //get installment amount
                            $inst=$row->mpesa_amount/$row->weeks;
                            $am=$inst*1.2;
                            //echo $am;
                            
                            
                            
                            $expe=$flo*$am;
                           
                            
                           
                            $arrears=$expe-$received;
                            
                        }
                        
                        //echo $installmentnumber;
                       
                       
                        
                       
                        if(($expe>$received AND $expe!=0))
                        {
                            $abutot=$abutot+$arrears;
                        }
                    
                   }    
                   //spencertotals
                       $spencertot=0;
                foreach ($spencer->result() as $row)  
                {  
                        if(isset($leo))
                       {
                           $de=$leo;
                           
                          
                          
                       }
                       
                       
                       $today=date('Y-m-d ', strtotime($de));
                    date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($today);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //if installment is greater than product length arrears is the same as balance
                         $received=$row->total;
                        if($installmentnumber>=$row->weeks)
                        {
                            $expe= $row->mpesa_amount*1.2;
                            $arrears=$expe-$received;
                        }
                        else
                        {
                                 echo "\t";
                            $flo=floor($installmentnumber);
                            
                           // echo $flo;
                            
                            //get installment amount
                            $inst=$row->mpesa_amount/$row->weeks;
                            $am=$inst*1.2;
                            //echo $am;
                            
                            
                            
                            $expe=$flo*$am;
                           
                            
                           
                            $arrears=$expe-$received;
                            
                        }
                        
                        //echo $installmentnumber;
                       
                       
                        
                       
                        if(($expe>$received AND $expe!=0))
                        {
                            $spencertot=$spencertot+$arrears;
                        }
                    
                   }    
                   
                 ?>
         
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>Name</th>
                  <th>Arrears</th>
                  <th>Percentage</th>
                  
                  <th>Details</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                   <tr>
                       <td>
                           Adila
                       </td>
                        <td>
                           <?php   echo number_format((float)$adilatot,2,'.','') ; ?>
                       </td>
                        <td>
                           <?php  
                               if($tot>0)
                               {
                                $re=$adilatot/$tot*100; 
                                echo number_format((float)$re,2,'.','') . "%";
                               }
                           ?>
                       </td>
                       <td>
                         <a style="color:blue" href="
                          <?php
                          $em=urlencode('adilachelimo@gmail.com');
                          echo base_url('admin/get_arrears/'.$em.'/Adila'); ?>">Details</a>
                       </td>
                   </tr>
                        <td>
                           Abraham
                       </td>
                        <td>
                           <?php   echo number_format((float)$abutot,2,'.','') ; ?>
                       </td>
                        <td>
                           <?php  
                            if($tot>0)
                               {    
                                $re=$abutot/$tot*100; 
                                echo number_format((float)$re,2,'.','') . "%";
                               }
                           ?>
                       </td>
                       <td>
                           <a style="color:blue" href="
                          <?php
                          $em=urlencode('abrahamkorir85@gmail.com');
                          echo base_url('admin/get_arrears/'.$em.'/Abraham'); ?>">Details</a>
                       </td>
                   <tr>
                        <td>
                           Spencer
                       </td>
                        <td>
                           <?php   echo number_format((float)$spencertot,2,'.','') ; ?>
                       </td>
                        <td>
                           <?php  
                              if($tot>0)
                               {
                                $re=$spencertot/$tot*100; 
                                echo number_format((float)$re,2,'.','') . "%";
                               }
                           ?>
                       </td>
                       <td>
                           <a style="color:blue" href="
                          <?php
                          $em=urlencode('ngenospencer90@gmail.com');
                          echo base_url('admin/get_arrears/'.$em.'/Spencer'); ?>">Details</a>
                       </td>
                   </tr>
                  
               </tbody>
            </table>
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5> Loan Arrears </h5>
          </div>
       
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>Name</th>
                  <th>Phone</th>
                  <th>National ID</th>
                  
                  <th>Occupation</th>
                  <th>Product</th>
                  <th>Disbursed Amount</th>
                   <th>Loan Officer</th>
                  <th>Due Amount</th>
                  <th>Start Date</th>
                  <th>Next Due Date</th>
                 
                  
                  <th>Status</th>
                  
                  <th>Received Total</th>
                   <th>Arrears</th>
                    <th>Payment Schedule</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                
                $zote=0;
                foreach ($h->result() as $row)  
                {  
                        if(isset($leo))
                       {
                           $de=$leo;
                           
                          
                          
                       }
                       
                       
                       $today=date('Y-m-d ', strtotime($de));
                    date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($today);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //if installment is greater than product length arrears is the same as balance
                         $received=$row->total;
                        if($installmentnumber>=$row->weeks)
                        {
                            $expe= $row->mpesa_amount*1.2;
                            $arrears=$expe-$received;
                        }
                        else
                        {
                                 echo "\t";
                            $flo=floor($installmentnumber);
                            
                           // echo $flo;
                            
                            //get installment amount
                            $inst=$row->mpesa_amount/$row->weeks;
                            $am=$inst*1.2;
                            //echo $am;
                            
                            
                            
                            $expe=$flo*$am;
                           
                            
                           
                            $arrears=$expe-$received;
                            
                        }
                        
                        //echo $installmentnumber;
                       
                       
                        
                       
                        if(($expe>$received AND $expe!=0))
                        {
                            $zote=$zote+$arrears;
                        
                 ?>
                <tr class="odd gradeX">
                    
                  
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                   <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                   <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>
                   <td class="center"><?php echo $row->officer_fname. ' '. $row->officer_lname;?></td>
                  <td class="center"><?php 
                                        $that=$row->mpesa_amount*1.2;
                                         $amount = number_format($that, 2, '.', '');
                                        echo $amount;
                                       
                                           ?></td>
                  <td class="center"><?php echo date("d-m-Y H:i:s", strtotime($row->mpesa_date));?></td>
                   <td class="center">
                       <?php
                        
                       
                       
                       //compare expected and received
                       date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($de);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //echo $installmentnumber;
                       // echo "\t";
                        $flo=floor($installmentnumber);
                        
                       // echo $flo;
                        
                        //get installment amount
                        $inst=$row->mpesa_amount/$row->weeks;
                        $am=$inst*1.2;
                        //echo $am;
                        
                        
                        
                        $expe=$flo*$am;
                        //echo $expe;
                        
                        $received=$row->total;
                        
                        
                       
                       
                       
                       
                       
                       
                       
                       
                      
                       if($row->product_id==1)
                       {
                       
                        date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                      if($received>=$expe)
                      {  
                          //get total amount
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fourthweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                        if($firstweek>=$today )
                        {
                            
                             echo date("d-m-Y H:i:s", strtotime($firstweek));
                            
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                           
                             echo date("d-m-Y H:i:s", strtotime($secondweek));
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($thirdweek));

                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                         elseif($today>=$fourthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                      }
                        
                      }
                       elseif($row->product_id==2)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                       
                       
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                        if($received>=$expe)
                      {  
                          //get total amount
                         
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4 AND  $wiki<5)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fifthweek));

                           }
                            elseif($wiki>=5 AND  $wiki<6)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                            elseif($wiki>=6 )
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                        if($firstweek>=$today)
                        {
                             echo date("d-m-Y H:i:s", strtotime($firstweek));
                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                             echo date("d-m-Y H:i:s", strtotime($secondweek));

                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($thirdweek));
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fourthweek));

                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($fifthweek));
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                        
                         elseif($today>=$sixthweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                           
                       }
                       }
                        elseif($row->product_id==3)
                       {
                           date_default_timezone_set('Africa/Nairobi');
                        //$today = date('Y-m-d H:i:s');
                        
                        
                       // $today =  date_format($de, 'Y-m-d H:i:s');
                        $startdate=date_create($row->mpesa_date);
                        //after seven days
                         // $date = "2015-11-17";
                          $firstwee=  date_add($startdate, date_interval_create_from_date_string('7 days'));
                          $firstweek =    date_format($firstwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $secondwee=  date_add($startdate, date_interval_create_from_date_string('14 days'));
                          $secondweek =    date_format($secondwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $thirdwee=  date_add($startdate, date_interval_create_from_date_string('21 days'));
                          $thirdweek =    date_format($thirdwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fouthwee=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                          $fourthweek =    date_format($fouthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $fifthwee=  date_add($startdate, date_interval_create_from_date_string('35 days'));
                          $fifthweek =    date_format($fifthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $sixthwee=  date_add($startdate, date_interval_create_from_date_string('42 days'));
                          $sixthweek =    date_format($sixthwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $seventhwee=  date_add($startdate, date_interval_create_from_date_string('49 days'));
                          $seventhweek =    date_format($seventhwee, 'Y-m-d H:i:s');
                          
                          $startdate=date_create($row->mpesa_date);
                          $eightwee=  date_add($startdate, date_interval_create_from_date_string('56 days'));
                          $eightweek =    date_format($eightwee, 'Y-m-d H:i:s');
                          
                                  if($received>=$expe)
                      {  
                          //get total amount
                          
                          $tugul    =$row->mpesa_amount*1.2;
                          $per=$tugul/$row->weeks;
                          
                           $wiki =$received/$per;
                           
                           if($wiki<1)
                           {
                                echo date("d-m-Y H:i:s", strtotime($firstweek));
                           }
                           
                           if($wiki>=1 AND $wiki<2)
                           {
                                echo date("d-m-Y H:i:s", strtotime($secondweek));
                           }
                           
                           elseif($wiki>=2 AND  $wiki<3)
                           {
                               echo date("d-m-Y H:i:s", strtotime($thirdweek));
                           }
                           
                           elseif($wiki>=3 AND  $wiki<4)
                           {
                                echo date("d-m-Y H:i:s", strtotime($fourthweek));
                           }
                            
                            elseif($wiki>=4 AND  $wiki<5)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($fifthweek));

                           }
                            elseif($wiki>=5 AND  $wiki<6)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($sixthweek));

                           }
                            elseif($wiki>=6 AND  $wiki<7)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($seventhweek));

                           }
                           
                           elseif($wiki>=7 AND  $wiki<8)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($eightweek));

                           }
                           
                            elseif($wiki>=8)
                           {
                                    echo date("d-m-Y H:i:s", strtotime($eightweek));

                           }
                          
                          
                      }
                      else
                      {
                          
                          
                        if($firstweek>=$today)
                        {
                          echo date("d-m-Y H:i:s", strtotime($firstweek));

                        }
                        elseif($secondweek>$today AND $today>$firstweek )
                        {
                            echo date("d-m-Y H:i:s", strtotime($secondweek));
                        }
                        elseif($thirdweek>$today AND $today>$secondweek)
                        {
                             echo date("d-m-Y H:i:s", strtotime($thirdweek));
                        }
                        elseif($fourthweek>$today AND $today>$thirdweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fourthweek));
                        }
                        
                         elseif($fifthweek>$today AND $today>$fourthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($fifthweek));
                        }
                        
                        elseif($sixthweek>$today AND $today>$fifthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($sixthweek));
                        }
                        
                        
                        elseif($seventhweek>$today AND $today>$sixthweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($seventhweek));
                        }
                        
                        elseif($eightweek>$today AND $today>$seventhweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($eightweek));
                        }
                        
                         elseif($today>=$eightweek)
                        {
                            echo date("d-m-Y H:i:s", strtotime($eightweek));
                        }
                      }   
                       }
                        ?>
                   </td>
                  
                  
                  
                 
                  <td>
                      <?php
                       // date_default_timezone_set('Africa/Nairobi');

                       //get the number of days
                        //$starda=date_create($row->mpesa_date);
                        $startdate=date_create($row->mpesa_date);
                        //$startdate = date_create($row->mpesa_date); //start date
                        
                         
                         //$date1 = new DateTime("2010-07-06");
                         $date2 = new DateTime($de);
                        //difference is here
                        $diff = $date2->diff($startdate)->format("%a");
                       // echo $diff;
                      
                        
                        //get current installment
                        $installmentnumber=$diff/7;
                        
                        //echo $installmentnumber;
                        echo "\t";
                        $flo=floor($installmentnumber);
                        
                       // echo $flo;
                        
                        //get installment amount
                        $inst=$row->mpesa_amount/$row->weeks;
                        $am=$inst*1.2;
                        //echo $am;
                        
                        
                        
                        $expe=$flo*$am;
                        //echo $expe;
                        
                        $received=$row->total;
                        if($received>=$expe)
                        {
                            echo "active";
                        }
                        else
                        {
                             echo "overdue";
  
                        }
                         
                        

                        
                      
                      ?>
                  </td>
                  
                  <td>
                      <?php
                         
                         if($row->received_date>=$row->mpesa_date)
                         {
                                  echo $row->total;
                                 
                         }
                         else
                         {
                             echo '0';
                         }
                       ?>
                  </td>
                  <td><?php  echo number_format((float)$arrears,2,'.','') ; ?></td>
                 <td><a style="color:blue" href="<?php  echo site_url("admin/paymentschedule/$row->customer_phone"); ?>">Payment Schedule</a></td>

                  
                </tr>
                 <?php }  
                }
                  ?>  
                <tr>
                    <td><b>TOTAL ARREARS</b></td>
                    <td><b><?php  echo number_format((float)$zote,2,'.','') ; ?></b></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>