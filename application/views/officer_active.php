<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Active Loans For My Clients</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Principal</th>
                  <th>Due Amount</th>
                  <th>Start Date</th>
                  <th>Due Date</th>
                   <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                  <td><?php echo $row->customer_id;?></td>
                  <td><?php echo $row->customer_fname;?></td>
                  <td class="center"> <?php echo $row->customer_lname;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_email;?></td>
                  <td class="center"><?php echo $row->principal;?></td>
                  <td class="center"><?php echo $row->principal*1.2;?></td>
                  <td class="center"><?php echo $row->start_date;?></td>
                  <td class="center">
                      <?php 
                        $date = date_create($row->start_date);
                        date_add($date, date_interval_create_from_date_string('30 days'));
                        echo date_format($date, 'Y-m-d H:i:s');
                        // echo $row->start_date;
                      ?>
                  </td>
                  <td class="center">
                      <?php 
                        $date = date_create($row->start_date);
                        date_add($date, date_interval_create_from_date_string('30 days'));
                        $duedate= date_format($date, 'Y-m-d H:i:s');
                        
                        date_default_timezone_set('Africa/Nairobi');
                        $now = date('Y-m-d H:i:s');
                        
                        $differenceFormat = '%a';
                        $datetime1 = date_create($now);
                        $datetime2 = date_create($duedate);
                        
                        $interval = date_diff($datetime1, $datetime2);
                        
                        if($interval->format($differenceFormat)<=30)
                        {
                            echo "Active";
                        }
                        else
                        {
                            echo "Overdue";
                        }
                        // echo $row->start_date;
                      ?>
                  </td>
                </tr>
                 <?php }  
                  ?>  
               
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php';  ?>