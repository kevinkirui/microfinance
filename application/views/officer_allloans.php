<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
    <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('officer/all_loans'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input required name="from" type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input required type="text" name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
      </form>
    
      
    </div>
  </div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All Loan Requests</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                  
                  <th>Email</th>
                  
                  <th>Occupation</th>
                  <th>Acc. NO</th>
                  <th>Product Name</th>
                   <th>Loan Reference Number</th>
                   <th>Loan Amount</th>
                  <th>Disbursed Amount</th>
                   <th>Created Date</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 <td><?php echo $row->customer_fname; ?></td>
                 <td><?php echo $row->customer_middlename;?></td>
                 <td><?php echo $row->customer_lname;?></td>
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                 <td class="center"><?php echo $row->customer_email;?></td>
                 

                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                   <td class="center"><?php echo $row->customer_accno;?></td>
                   <td class="center"><?php echo $row->product_name;?></td>
                   <td class="center kod"><?php echo $row->ref_no;?></td>
                  <td class="center"><?php echo $row->request_amount;?></td>
                  <td class="center">
                    
                       <?php
                        if($row->mpesa_disbursed_code!=NULL)
                        {
                           echo $row->request_amount;
                        }
                        
                        ?>
                   </td>
                   <td>
                    <?php 
                        //echo $row->requested_date;  
                        echo date("d-m-Y H:i:s", strtotime($row->requested_date));
                     ?>               
                   </td>
                   

                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>