<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-warning">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
              if(isset($messo))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $messo;
                ?>
               </div>
            <?php
              }
                  
            
            ?>
            <?php echo validation_errors(); ?>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Admin Change Password</h5>
        </div>
        <div class="widget-content nopadding">
          <form  method="post"  action="<?php echo base_url('admin/change_process')?>" class="form-horizontal">
       
            <div class="control-group">
              <label class="control-label">Old Password :</label>
              <div class="controls">
                <input required type="password" name="old" class="span9" placeholder="Old Password" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">New Password :</label>
              <div class="controls">
                <input required type="password" name="password" class="span9" placeholder="New Password" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Confirm New Password</label>
              <div class="controls">
                <input type="password" name="c_password" class="span9" placeholder="Confirm New Password" />
              </div>
            </div>
             
            
            
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Change</button>
            </div>
          </form>
        
       
    </div>
  </div>
</div></div>
<!--Footer-part-->
<?php include 'footer.php'; ?>