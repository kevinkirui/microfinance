<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
               </div>
            <?php
              }
                  
            
            ?>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Reason for rejection</h5>
        </div>
        <div class="widget-content nopadding">
          <form  method="post"  action="<?php echo base_url('admin/approve_reject')?>" class="form-horizontal">
            <?php
              if(isset($id))
              {
                  ?>
                 <input type="hidden" name="id" value="<?php echo $id; ?>"/>
              <?php
              }
            ?>
            <div class="control-group">
              <label class="control-label">Reason :</label>
              <div class="controls">
                <textarea type="text" name="reason" class="span9" placeholder="Reason" >
                </textarea>
              </div>
            </div>
            
            
            
            <div class="form-actions">
              <button type="submit" class="btn btn-danger">Reject</button>
            </div>
          </form>
        
       
    </div>
  </div>
</div></div>
<!--Footer-part-->
<?php include 'footer.php'; ?>