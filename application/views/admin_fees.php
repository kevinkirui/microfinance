
<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
   <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('admin/registration_fees'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input  name="from" required type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input  type="text" required name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
    
      </form>
       <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           //capture from and to
           if(isset($from))
           {
               $from=base64_encode($from);
               $from=$from;
           }
           
           if(isset($to))
           {
               $to=base64_encode($to);
               $to=$to;
           }
         
         ?>
         <a href="<?php echo base_url('Admin/registration_fees_export/'.$from.'/'.$to); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
    
      
    </div>
  </div>
  <div class="container-fluid">
    
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Registration Fees</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Name</th>
                  <th>Phone</th>
                  <th>National ID</th>
                  
                  
                  <th>Amount</th>
                
                  <th>Date</th>
                  
                    
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                $zote=0;
                foreach ($h->result() as $row)  
                {  
                       
                 ?>
                <tr class="odd gradeX">
                  
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                   <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                 
                  <td class="center"><?php echo $row->registration_amount;?></td>
                  
                  <td class="center"><?php echo date("d-m-Y H:i:s", strtotime($row->registration_date));?></td>
                 </tr>
                 <?php
                    $zote=$zote+$row->registration_amount;
                    }
                 ?>
                <tr>
                    <td><b>TOTAL RECEIVED</b></td>
                    <td><b><?php
                              
                                
                               echo number_format((float)$zote,2,'.','') ;
                              ?></b></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>