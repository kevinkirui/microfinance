<?php include 'admin_header.php'; ?>
<!-- CONTENT AREA -->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
        <?php
              if(isset($success))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $success;
                ?>
               </div>
            <?php
              }
                  
            
            ?>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
           
          <h5>Add admin</h5>
        </div>
        <div class="widget-content nopadding">
          <form  method="post"  action="<?php echo base_url('admin/registerUser')?>" class="form-horizontal">
       
            <div class="control-group">
              <label class="control-label">Admin First Name :</label>
              <div class="controls">
                <input required type="text" name="admin_fname" class="span9" placeholder="Admin First Name" />
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Admin Email :</label>
              <div class="controls">
                <input required type="email" name="admin_email" class="span9" placeholder="Email" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Admin Password</label>
              <div class="controls">
                <input type="password" name="admin_password" class="span9" placeholder="Password" />
              </div>
            </div>
             
            
            
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Add Admin</button>
            </div>
          </form>
        
       
    </div>
  
<!--Footer-part-->
<?php include 'footer.php'; ?>