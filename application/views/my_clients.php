<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="alert alert-success" style="font-size:18px">
             <?php
              if(isset($message))
              { ?>
              <div class="alert alert-success">
                <?php
                  echo $message;
                ?>
              
            <?php
              }
                  
            
            ?>
            </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>My Clients</h5>
          </div>
          <div class="widget-content nopadding">
           <div class="table-responsive">
            
            <table class="table table-responsive table-bordered table-striped ">
             
              <thead>
                <tr>
                  
                 
                  <th>Name</th>
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Occupation</th>
                   <th>Guarantor</th>
                   <th>Document</th>
                  <th>Loan Status</th>
                  <th>Product</th>
                  <th>Amount</th>
                  <th>Loan action</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
            <form  method="post"  action="<?php echo base_url('officer/request_loan')?>" class="form-horizontal">
                 <input name="customer_id" type="hidden" value="<?php echo $row->customer_id_number;?>"/>
                 <input name="customer_phone" type="hidden" value="<?php echo $row->customer_phone;?>"/>
                <tr class="odd gradeX">
                 
              
                  <td class="center"> <?php echo $row->customer_fname ."\t".$row->customer_lname;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td><a style="color:blue" href="<?php  echo site_url("officer/guarantor_details/$row->customer_phone"); ?>">Guarantor Details</a></td>
                  <?php
                     if($row->customer_document)
                     {
                  ?>
                  <td><a style="color:blue" href="<?php  echo base_url("uploads/$row->customer_document"); ?>">Document</a></td>
                  <?php
                     }
                  
                 
                  else
                  { ?>
                   <td></td>
                  <?php
                  }
                  ?>
                  <td class="center"><?php echo $row->status_name;?></td>
                  <td class="center">
                      <div class="controls">
                    <select name="product" required >
                     <option  value=""></option>

                <?php
                                 foreach ($level->result_array() as $kev)
 
                                    { ?>
              
                                <option  value="<?php echo $kev['product_id']; ?>"><?php echo $kev['product_name']; ?></option>
                                 <?php
                                    }
                                 ?>
                       </select>
                    </div>
                      
                  </td>
                 
                   <td class="center">
                      <?php if($row->loan_id==1) { ?>
                     
                       <input required name="amount" type="number" min="1000" max="25000"/>
                       <?php
                      }
                     
                    ?>
                   </td>
                     
                 
                  <td class="center"><?php if($row->loan_id==1)
                      { ?>
          
                    <button type="submit" class="btn btn-success"><span class="lolz">Request </span></button>
                             
                <?php
                      }
                    
                      ?></td>
                 
                </tr>
              </form>
                 <?php }  
                  ?>  
               
              </tbody>
             
            </table>
           </div>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php';  ?>