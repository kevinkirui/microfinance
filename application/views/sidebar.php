<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    
         
        
    <li class="<?php if($this->uri->segment(2)=="truestatus" or $this->uri->segment(2)=="truestatus" ){echo "active";}?>"><a href="<?php echo base_url('admin/truestatus')?>"><i class="icon icon-home"></i> <span>Loan Status</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="activeloans"){echo "active";}?>"> <a href="<?php echo base_url('admin/activeloans')?>"><i class="icon icon-signal"></i> <span>Active Loans</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="get_arrears" or $this->uri->segment(2)=="loan_arrears" ){echo "active";}?>"><a href="<?php echo base_url('admin/loan_arrears')?>"><i class="icon icon-pencil"></i> <span>Loan Arrears</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="all_disbursed" or $this->uri->segment(2)=="all_disbursed" ){echo "active";}?>"><a href="<?php echo base_url('admin/all_disbursed')?>"><i class="icon icon-file"></i> <span>Disbursed Loans</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="rejected"){echo "active";}?>"> <a href="<?php echo base_url('admin/rejected')?>"><i class="icon icon-th-list"></i> <span>Rejected Loans</span> <span class="label label-important"></span></a>
    <li class="<?php if($this->uri->segment(2)=="admin_balance" or $this->uri->segment(2)=="admin_balance" ){echo "active";}?>"><a href="<?php echo base_url('admin/admin_balance')?>"><i class="icon icon-tint"></i> <span>Loan Balance</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="mpesa_received" or $this->uri->segment(2)=="mpesa_received" ){echo "active";}?>"><a href="<?php echo base_url('admin/mpesa_received')?>"><i class="icon icon-inbox"></i> <span>Mpesa Received</span></a> </li>

    <li class="<?php if($this->uri->segment(2)=="mpesa_repayments" or $this->uri->segment(2)=="view_bankstatement" ){echo "active";}?>"><a href="<?php echo base_url('admin/mpesa_repayments')?>"><i class="icon icon-inbox"></i> <span>Mpesa Repayments</span></a> </li>

    <li class="<?php if($this->uri->segment(2)=="all_loans" or $this->uri->segment(2)=="all_loans" ){echo "active";}?>"><a href="<?php echo base_url('admin/all_loans')?>"><i class="icon icon-info-sign"></i> <span>All Loan Requests</span></a> </li>
    
    <li class="<?php if($this->uri->segment(2)=="registration_fees"){echo "active";}?>"> <a href="<?php echo base_url('admin/registration_fees')?>"><i class="icon icon-th-list"></i> <span>Registration Fees</span> <span class="label label-important"></span></a>

    
    <li class="<?php if($this->uri->segment(2)=="pending_approval"){echo "active";}?>"> <a href="<?php echo base_url('admin/pending_approval')?>"><i class="icon icon-th-list"></i> <span>Loans Pending Approval</span> <span class="label label-important"></span></a>

    <li class="<?php if($this->uri->segment(2)=="awaiting_disbursement"){echo "active";}?>"> <a href="<?php echo base_url('admin/awaiting_disbursement')?>"><i class="icon icon-th-list"></i> <span>Loans Pending Disbursement</span> <span class="label label-important"></span></a>


    <li class="<?php if($this->uri->segment(2)=="loans_duetoday" or $this->uri->segment(2)=="loans_duetoday" ){echo "active";}?>"><a href="<?php echo base_url('admin/loans_duetoday')?>"><i class="icon icon-home"></i> <span>Loans Due Today</span></a> </li>

    <li class="<?php if($this->uri->segment(2)=="receive_money"){echo "active";}?>"><a href="<?php echo base_url('admin/receive_money')?>"><i class="icon icon-th"></i> <span>Receive Money</span></a></li>

    <li class="<?php if($this->uri->segment(2)=="add_admin"){echo "active";}?>"><a href="<?php echo base_url('admin/add_admin')?>"><i class="icon icon-th"></i> <span>Add Admin</span></span></a></li>
    <li class="<?php if($this->uri->segment(2)=="all_admins"){echo "active";}?>"><a href="<?php echo base_url('admin/all_admins')?>"><i class="icon icon-th"></i> <span>All Admins</span></span></a></li>

   


    <li class="<?php if($this->uri->segment(2)=="all_officers"){echo "active";}?>"><a href="<?php echo base_url('admin/all_officers')?>"><i class="icon icon-th"></i> <span>Loan Officers</span></a></li>
    <li class="<?php if($this->uri->segment(2)=="add_officer"){echo "active";}?>"><a href="<?php echo base_url('admin/add_officer')?>"><i class="icon icon-th"></i> <span>Add Loan Officer</span></a></li>
 
   
   
  </ul>
</div>