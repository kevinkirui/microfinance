
<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
   <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('admin/sms_list'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input  name="from" required type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input  type="text" required name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
    
      </form>
       <div class="span2">
         <br>
         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">How to Pay</button>

<!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">How to top up Credit</h4>
                  </div>
                  <div class="modal-body">
                   <p> Using you MPESA-enabled phone, select "Buy Goods and Services" from the M-PESA menu</p>
                     <p>Enter Till Number: 771828</p>
                     <p>Enter the Amount of credits you want to buy</p>
                     <p>Confirm that all the details are correct and press Ok</p>
                     <p>Check your statement to see your payment. Your account will also be updated in 1 minute.</p>
                     <p>You Will receive an SMS from Us confirming your payments</p>
                      <p>Done</p>
                                      </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
     </div>
    
      
    </div>
  </div>
  <div class="container-fluid">
    
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All SMS</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>SmsID</th>
                  <th>SystemID</th>
                  <th>Phone</th>
                  
                  
                  <th>Message</th>
                  <th>Status</th>
                   <th>Balance</th>
                
                  <th>Date</th>
                  
                    
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                $zote=0;
                foreach ($h->result() as $row)  
                {  
                       
                 ?>
                <tr class="odd gradeX">
                  
                  <td><?php echo $row->message_id;?></td>
                   <td class="center"><?php echo $row->system_id;?></td>
                  <td class="center"><?php echo $row->phone;?></td>
                 
                 
                  <td class="center"><?php echo $row->message;?></td>
                  <td class="center"><?php echo $row->status;?></td>
                   <td class="center"><?php echo $row->balance;?></td>
                  
                  <td class="center"><?php echo date("d-m-Y H:i:s", strtotime($row->date));?></td>
                 </tr>
                 <?php
                   
                    }
                 ?>
                <tr>
                    
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>