<?php include 'admin_header.php' ?>
 
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid" style="margin-bottom:-20px;">
       
    <div class="row-fluid">
    <div class="span3">
        <form class="form-inline" method="post" action="<?php echo base_url('admin/all_disbursed'); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input required name="from" type="text" id="from" placeholder="From"/>
              </div>
            </div>
    </div>
     <div class="span3">
            <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input required type="text" name="to" id="to" placeholder="To" />
              </div>
            </div>
     </div>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
     </div>
      </form>
      <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           //capture from and to
           if(isset($from))
           {
               $from=base64_encode($from);
               $from=$from;
           }
           
           if(isset($to))
           {
               $to=base64_encode($to);
               $to=$to;
           }
         
         ?>
         <a href="<?php echo base_url('Admin/all_disbursed_export/'.$from.'/'.$to); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
     <div class="span2">
          <br>
         <a href="<?php echo base_url('Admin/disbursed_officer/'.$from.'/'.$to); ?>"><button type="submit" class="btn btn-warning">Officer Disbursed</button></a>
    
     </div>
    </div>
  </div>
  <div class="container-fluid">
  
  
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All Disbursed Loans</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              
                  
                  

                  
               
              <tbody>
               
                <tr class="odd gradeX">
                 <thead>
                  
                
                                

                 <td>
                 <table class="table table-bordered table-striped">
                    <thead>
                <tr>
                 <th>Branch Name</th>
                 <th>Total Amount</th>
                  
                  <th>Customer Name</th>
                  <th>Mobile Number</th>
                  
                  <th>ID number</th>
                  <th>Branch</th>
                  <th>Loan Officer</th>
                  <th>Product Name</th>
                  <th>Loan Ref NO</th>
                  <th>Mpesa Ref</th>
                  <th>Disbursed Amount</th>
                  

                  
                </tr>
              </thead>
                <tr>
                 
                   <td rowspan="<?php 
                   if(isset($regionthree))
                       { 
                        if($regionthree>0)
                        {
                         
                          echo $regionthree; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>REGION 3</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($regionthree))
                       { 
                        if($regionthree>0)
                        {
                         
                          echo $regionthree; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td>
                             <?php
                               if(isset($regionthreesum))
                               {
                                 
                                  foreach ($regionthreesum->result() as $row)  
                                  {  
                                      $firstsum=$row->regionthreesum;
                                      echo $firstsum;
                                  }
                               }
                             ?></td>
                       
                        </tr>
                        
                    </table>
                   </td>
                  
                   <?php
                
                foreach ($three->result() as $row)  
                {  
                 ?>
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->location_name;?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>

                
                   
                  </tr>
                  <?php } 
                  
                  //see below
                  ?>  
                  
                   <tr>
                 
                   <td rowspan="<?php 
                   if(isset($kimumucount))
                       { 
                        if($kimumucount>0)
                        {
                         
                          echo $kimumucount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>KIMUMU</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($kimumucount))
                       { 
                        if($kimumucount>0)
                        {
                         
                          echo $kimumucount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td><?php
                               if(isset($kimumusum))
                               {
                                 
                                  foreach ($kimumusum->result() as $row)  
                                  {  
                                      $secondsum=$row->kimumusum;
                                      echo $secondsum;
                                  }
                               }
                             ?></td>
                       
                        </tr>
                        
                    </table>
                   </td>
                  
                   <?php
                
                foreach ($kimumu->result() as $row)  
                {  
                 ?>
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->location_name;?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>

                 
                   
                  </tr>
                  <?php } 
                  
                  //see below
                  ?>  
                  <tr>
                 
                   <td rowspan="<?php 
                   if(isset($mailicount))
                       { 
                        if($mailicount>0)
                        {
                         
                          echo $mailicount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>">
                    <table>
                        <tr>
                         <td>MAILI NNE</td>
                       
                        </tr>
                        
                    </table>
                   </td>
                   
                   <td rowspan="<?php 
                   if(isset($mailicount))
                       { 
                        if($mailicount>0)
                        {
                         
                          echo $mailicount; 
                        }
                        else
                        {
                            echo 1;
                        }
                       } ?>" >
                    <table>
                        <tr>
                         <td>
                             <?php
                               if(isset($mailisum))
                               {
                                 
                                  foreach ($mailisum->result() as $row)  
                                  {  
                                      $thirdsum=$row->mailisum;
                                      echo  $thirdsum;
                                  }
                               }
                             ?>
                         </td>
                       
                        </tr>
                        
                    </table>
                   </td>
                  
                   <?php
                
                foreach ($maili->result() as $row)  
                {  
                 ?>
                
                   
                 <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->location_name;?></td>
                  <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center kod"><?php echo $row->ref_no;?></td>

                  <td class="center kod"><?php echo $row->mpesa_code;?></td>
                  <td class="center"><?php echo $row->mpesa_amount;?></td>

                 
                   
                  </tr>
                  <?php } 
                  
                  //see below
                  ?>  
                 
                   
                   
                  </table> 
                 </td>
                </tr>
                
                <!-- new row-->
                
                
                
                
                
              </tbody>
            </table>
               <table class="table table-bordered table-striped">
                    <tr><td><b>TOTAL</b></td>
                    <td><b><?php
                                  $toshsum=$firstsum+ $secondsum+ $thirdsum; 
                                  echo number_format($toshsum, 2, '.', '');
                                  
                                  ?></b></td></tr>
                </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>