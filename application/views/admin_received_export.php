<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=mpesareceived.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Name</th>
                  <th>Phone</th>
                  <th>National ID</th>
                  
                  
                  <th>Amount</th>
                   <th>Transaction Code</th>
                
                  <th>Date</th>
                  
                    
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                $zote=0;
                foreach ($h->result() as $row)  
                {  
                       
                 ?>
                <tr class="odd gradeX">
                  
                  <td><?php echo $row->customer_fname."\t".$row->customer_lname;?></td>
                   <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                 
                  <td class="center"><?php echo $row->received_amount;?></td>
                  <td class="center"><?php echo $row->received_transaction_id;?></td>
                  
                  <td class="center"><?php echo date("d-m-Y H:i:s", strtotime($row->received_date));?></td>
                 </tr>
                 <?php
                    $zote=$zote+$row->received_amount;
                    }
                 ?>
                <tr>
                    <td><b>TOTAL RECEIVED</b></td>
                    <td><b><?php
                              
                                
                               echo number_format((float)$zote,2,'.','') ;
                              ?></b></td>
                </tr>
              </tbody>
            </table>