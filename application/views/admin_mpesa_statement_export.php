<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=mpesa_statement.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  
                 
                  <th>Debit</th>
                  <th>Credit</th>
                  
                  <th>Mpesa Code</th>
                  <th>Transaction Date</th>
                  
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                $disbursed=0;
                $count=0;
                if(isset($h)){
                foreach ($h->result() as $row)  
                {  
                 $count++;
                 if($row->mpesa_type=='D' && $count==1 ){
                 ?>
                    <tr class="odd gradeX">
                     
                    
                       <td>_</td>
                        <td><?php 
                     
                         $disbursed= -$row->pesa;
                         //echo $disbursed;
                         echo number_format($disbursed, 2, '.', '');
                      
                       ?></td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center"><?php
                
                 echo date("d-m-Y H:i:s", strtotime($row->dato));
                 ?>
                 </td>


                  </tr> 
                     

                 
                 <?php 
                  }
                  
                  elseif($row->mpesa_type=='D' && $count>1 ){
                 ?>
                    <tr class="odd gradeX">
                     
                    
                       <td>_</td>
                        <td><?php 
                     
                         $disbursed= -$row->pesa;
                         echo $disbursed;
                      
                       ?></td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center"><?php
                
                 echo date("d-m-Y H:i:s", strtotime($row->dato));
                 ?>
                 </td>


                  </tr> 
                  
                  <tr class="odd gradeX">
                     <?php 
                     
                         $disbursed= -$row->pesa+(-$disbursed);
                        // echo $disbursed;
                        if ($disbursed>0)
                        {
                       ?>
                      
                    
                       <td><?php echo $disbursed;  ?></td>
                        <td>_</td>
                      <?php
                        }
                        else
                        {
                      ?>
                        <td>_</td>
                        <td><?php echo $disbursed;  ?></td>
                       <?php
                        }
                       ?>


                     <td class="kod">_</td>

                    
                 <td class="center">_
                 </td>


                  </tr> 

                     

                 
                 <?php 
                  }
                
                  
                  else
                  {
                      
                      ?>
                   <tr class="odd gradeX">
                             
                     
                     <td><?php 
                     
                         $received=$row->pesa;
                         echo $received;
                      
                       ?></td>
                       <td>_</td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center">
                     <?php
                                      echo date("d-m-Y H:i:s", strtotime($row->dato));

                     
                     ?></td>
                    
                 </tr> 
                   <tr class="odd gradeX">
                        <?php 
                            $disbursed=$received+$disbursed;
                           // echo $disbursed;
                                    
                            
                     
                      if ($disbursed>0)
                        {
                       ?>
                      
                    
                       <td><?php echo $disbursed;  ?></td>
                        <td>_</td>
                      <?php
                        }
                        else
                        {
                      ?>
                        <td>_</td>
                        <td><?php echo $disbursed;  ?></td>
                       <?php
                        }
                       ?>
                    


                       <td class="kod">
                           _
                       </td>

                    
                 <td class="center">
                  _  
                 </td>
                    
                 </tr> 
               <?php 
                 }
                }
                }
                  ?>  
              </tbody>
            </table>