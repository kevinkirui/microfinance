<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Rejected Clients</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Occupation</th>
                  <th>Ref No.</th>
                  <th>Rejection Reason</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                 if(isset($h))
                {
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                  <td><?php echo $row->customer_id;?></td>
                  <td><?php echo $row->customer_fname;?></td>
                  <td class="center"> <?php echo $row->customer_lname;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_email;?></td>
                   <td class="center"><?php echo $row->customer_occupation;?></td>
                   <td class="center"><?php echo $row->ref_no;?></td>
                    <td class="center"><?php echo $row->rejected_reason;?></td>
                </tr>
                 <?php }  
                }
                  ?>  
               
              </tbody>
            </table>
             <?php
                     if(isset ($message)) {
                   ?>
              <div class="alert alert-warning" style="text-align:center">
                   <strong><?php echo $message; ?></strong>
              </div>
              <?php     
                  }
                 ?> 
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php';  ?>