<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Loans Pending Approval</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Created Date</th>
                  <th>Customer Name</th>
                 
                  <th>National ID</th>
                  <th>Phone</th>
                  <th>Product</th>
                  <th>Ref No</th>
                   <th>Amount</th>
                   <th>Interest</th>
                  <th>Status</th>
                  <th>Branch</th>
                  <th>Occupation</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                if(isset($h))
                {
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                  <td><?php  echo date("d-m-Y H:i:s", strtotime($row->requested_date));?></td>
                  <td><?php echo $row->customer_fname."\t ".$row->customer_lname ?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                  <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->product_name;?></td>
                  <td class="center " style="text-transform: uppercase;"><?php echo $row->ref_no;?></td>
                  <td class="center"><?php echo $row->request_amount;?></td>
                  <td class="center"><?php 
                                          //echo $row->request_amount*0.2;
                                           $inte= $row->request_amount*0.2;
                                          echo number_format($inte, 2, '.', '');
                                          
                                          ?></td>
                  <td class="center"><?php echo $row->status_name;?></td>
                    <td class="center"><?php echo $row->location_name;?></td>
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  </tr>
                 <?php 
                }
                }  
                  ?>  
               
              </tbody>
            </table>
              <?php
                     if(isset ($message)) {
                   ?>
              <div class="alert alert-warning" style="text-align:center">
                   <strong><?php echo $message; ?></strong>
              </div>
              <?php     
                  }
                 ?> 
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>