<?php include 'officer_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All Customers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Branch</th>
                  <th>First Name</th>
                
                  <th>Last Name</th>
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                  <th>Loan Officer</th>
                  <th>Loan Officer Number</th>
                  <th>Occupation</th>
                  <th>Document</th>
                   <th>Guarantor</th>
                  <th>Creation Date</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 <td><?php echo $row->location_name;?></td>
                 <td><?php echo $row->customer_fname; ?></td>
                
                 <td><?php echo $row->customer_lname;?></td>
                 <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->officer_phone;?></td>
               
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                   <?php
                     if($row->customer_document)
                     {
                  ?>
                  <td><a style="color:blue" href="<?php  echo base_url("uploads/$row->customer_document"); ?>">Document</a></td>
                  <?php
                     }
                  
                 
                  else
                  { ?>
                   <td></td>
                  <?php
                  }
                  ?>
                   <td><a style="color:blue" href="<?php  echo site_url("officer/guarantor_details/$row->customer_phone"); ?>">Guarantor Details</a></td>

                  <td class="center"><?php 
                 // echo $row->added_date;
                   echo date("d-m-Y H:i:s", strtotime($row->added_date));
                  ?></td>
                  
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>