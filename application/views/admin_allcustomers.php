<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <?php
      if(isset($message))
      {
          ?>
          <div class="alert-success">
            <?php
            
              echo $message;
            
            ?>
              
          </div>
     <?php
          
      }
    
    
    ?>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>All Customers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Branch</th>
                  <th>Name</th>
                  
                  
                  
                  <th>Phone</th>
                  <th>ID </th>
                  <th>Loan Officer</th>
                  <th>Loan Officer Phone</th>
                  <th>Occupation</th>
                   <th>Guarantor</th>
                   <th>Documents</th>
                  <th>Creation Date</th>
                  <th>Edit</th>
                  <th>Transfer Customer</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 <td><?php echo $row->location_name;?></td>
                 <td><?php echo $row->customer_fname.' '.$row->customer_lname; ?></td>
                
                 
                 <td class="center"><?php echo $row->customer_phone;?></td>
                  <td class="center"><?php echo $row->customer_id_number;?></td>
                 <td class="center"><?php echo $row->officer_fname."\t".$row->officer_lname;?></td>
                  <td class="center"><?php echo $row->officer_phone;?></td>
               
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td><a style="color:blue" href="<?php  echo site_url("admin/guarantor_details/$row->customer_phone"); ?>">Guarantor Details</a></td>
                   <?php
                     if($row->customer_document)
                     {
                  ?>
                  <td><a style="color:blue" href="<?php  echo base_url("uploads/$row->customer_document"); ?>">Document</a></td>
                  <?php
                     }
                  
                 
                  else
                  { ?>
                   <td></td>
                  <?php
                  }
                  ?>


                  <td class="center"><?php 
                 // echo $row->added_date;
                   echo date("d-m-Y H:i:s", strtotime($row->added_date));
                  ?></td>
                  <td class="center"> 
                                          <a style="color:red" Onclick="return confirm('Are you sure you want to edit this record?')" href="<?php  echo site_url("admin/edit_customer/$row->customer_id"); ?>">Edit</a>

                  </td>
                  <td class="center"> 
                                          <a style="color:red" Onclick="return confirm('Are you sure you want to transfer this client?')" href="<?php  echo site_url("admin/transfer_customer/$row->customer_id"); ?>">Transfer Customer</a>

                  </td>
                  
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>