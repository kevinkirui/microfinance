<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
  <div class="container-fluid" style="margin-bottom:-20px;">
    <div class="row-fluid">
        <?php
           if(isset($phone))
           {
               $phone=$phone;
           }
        
        ?>
     <div class="span3">
         <form class="form-inline" method="post" action="<?php echo base_url('admin/view_statement/'.$phone); ?>">
          <div class="control-group">
              <label class="control-label">From :</label>
              <div class="controls">
                <input required name="from" type="text" id="from" placeholder="From"/>
              </div>
            </div>
        
    </div>
     <div class="span3">
          <div class="control-group">
              <label class="control-label">To :</label>
              <div class="controls">
                <input required type="text" name="to" id="to" placeholder="To" />
              </div>
            </div>
           
     </div>
      <div class="span2">
          <label class="control-label"></label>
         <br>
         <button type="submit" class="btn btn-primary">Submit</button>
         
     </div>
      </form>
     <div class="span2">
         <label class="control-label"></label>
         <br>
         <?php
           //capture from and to
           if(isset($phone))
           {
               
               $phone=$phone;
           }
           
           
         ?>
         <a href="<?php echo base_url('Admin/view_statement_export/'.$phone); ?>"><button type="submit" class="btn btn-success">Export</button></a>
     </div>
      
    </div>
  </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Mpesa Repayments</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                 
                  <th>Phone</th>
                  <th>Amount Sent</th>
                  <th>Amount Received</th>
                  
                  <th>Mpesa Code</th>
                  <th>Transaction Date</th>
                  
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                if(isset($h)){
                foreach ($h->result() as $row)  
                {  
                 if($row->mpesa_type=='D'){
                 ?>
                    <tr class="odd gradeX">
                     <td><?php 
                      
                          echo $row->phone;
                         ?>
                       </td>
                     <td><?php 
                     
                         echo $row->pesa;
                      
                       ?></td>
                       <td>_</td>
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center"><?php
                
                 echo date("d-m-Y H:i:s", strtotime($row->dato));
                 ?>
                 </td>


                  </tr> 
                     

                 
                 <?php 
                  }
                  else
                  {
                      
                      ?>
                   <tr class="odd gradeX">
                              <td><?php 
                      
                          echo $row->phone;
                         ?>
                       </td>
                      <td>_</td>
                     <td><?php 
                     
                         echo $row->pesa;
                      
                       ?></td>
                      
                    


                     <td class="kod"><?php 
                      
                        
                          echo $row->code;

                      
                       ?></td>

                    
                 <td class="center">
                     <?php
                                      echo date("d-m-Y H:i:s", strtotime($row->dato));

                     
                     ?></td>
                    
                 </tr> 
               <?php 
                 }
                }
                }
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part--><?php include 'footer.php'; ?>
