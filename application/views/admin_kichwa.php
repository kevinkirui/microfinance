<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Microfinance</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/fullcalendar.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('css/matrix-media.css'); ?>" />
<link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url('css/jquery.gritter.css'); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="#">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome <?php if(isset($jina)){echo $jina;} ?></span><b class="caret"></b></a>
      
    </li>
    <li class="<?php if($this->uri->segment(2)=="dashboard" or $this->uri->segment(2)=="user_login_process" ){echo "active";}?>"><a href="<?php echo base_url('admin/dashboard')?>"><i class="icon icon-home"></i> <span>DASHBOARD</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="approve" or $this->uri->segment(2)=="approve" ){echo "active";}?>"><a href="<?php echo base_url('admin/approve')?>"><i class="icon icon-home"></i> <span class="label label-important"><?php if(isset($requestcount)){ echo $requestcount; } ?></span><span>LOAN REQUESTS</span></a> </li>
    <li class="<?php if($this->uri->segment(2)=="disburse" or $this->uri->segment(2)=="disburse" ){echo "active";}?>"><a href="<?php echo base_url('admin/disburse')?>"><i class="icon icon-home"></i><span class="label label-important"><?php if(isset($disbursecount)){ echo $disbursecount; } ?> </span><span>DISBURSE LOANS</span></a> </li>
 
    <li class="<?php if($this->uri->segment(2)=="activeloans" or $this->uri->segment(2)=="activeloans" ){echo "active";}?>"><a href="<?php echo base_url('admin/all_customers')?>"><i class="icon icon-home"></i> <span>CUSTOMERS</span></a> </li>
     <li class="<?php if($this->uri->segment(2)=="all_loans" or $this->uri->segment(2)=="all_loans" ){echo "active";}?>"> <a href="<?php echo base_url('admin/all_loans')?>"  ><i class="icon icon-th-list"></i> <span>REPORTS</span> <span class="label label-important">10</a></li>

    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Change Password</span></a></li>
    <li class=""><a title="" href="logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>