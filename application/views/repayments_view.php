<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Mpesa Repayments</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Branch</th>
                  <th>Customer Name</th>
                 
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                
                 
                  <th>Occupation</th>
                  
                  <th>Mpesa Repayments</th>
                  <th>Mpesa Statement</th>
                  <th>Creation Date</th>
                  <th>Transfer</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 <td><?php echo $row->location_name;?></td>
                 
                 <td><?php echo $row->customer_fname."\t".$row->customer_middlename."\t".$row->customer_lname;?></td>
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                  <td><a href="  <?php  echo site_url("admin/view_statement/$row->customer_phone"); ?>"><button type="submit" class="btn btn-success">View Repayments</button></a></td>
                  <td><a href="  <?php  echo site_url("admin/view_bankstatement/$row->customer_phone"); ?>"><button type="submit" class="btn btn-warning">View Statement</button></a></td>

                  <td class="center"><?php echo date("d-m-Y H:i:s", strtotime($row->added_date));?></td>
                  <td class="center">
                  <a style="color:red" Onclick="return confirm('Are you sure you want to transfer funds?')" href="<?php  echo site_url("admin/view_transfer/$row->customer_phone"); ?>">Transfer</a></td>

                  
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>