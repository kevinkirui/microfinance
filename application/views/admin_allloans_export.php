<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=loanrequests.xls");
header("Pragma: no-cache");
header("Expires: 0");
?> 
 <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                  
                  <th>Email</th>
                  
                  <th>Occupation</th>
                  <th>Acc. NO</th>
                  <th>Product Name</th>
                   <th>Loan Reference Number</th>
                   <th>Loan Amount</th>
                  <th>Disbursed Amount</th>
                   <th>Created Date</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
                 <td><?php echo $row->customer_fname; ?></td>
                 <td><?php echo $row->customer_middlename;?></td>
                 <td><?php echo $row->customer_lname;?></td>
                 <td class="center"><?php echo $row->customer_phone;?></td>
                 <td class="center"><?php echo $row->customer_id_number;?></td>
                 
                 <td class="center"><?php echo $row->customer_email;?></td>
                 

                 
                  <td class="center"><?php echo $row->customer_occupation;?></td>
                   <td class="center"><?php echo $row->customer_accno;?></td>
                   <td class="center"><?php echo $row->product_name;?></td>
                   <td class="center kod"><?php echo $row->ref_no;?></td>
                  <td class="center"><?php echo $row->request_amount;?></td>
                  <td class="center">
                    
                       <?php
                        if($row->mpesa_disbursed_code!=NULL)
                        {
                           echo $row->request_amount;
                        }
                        
                        ?>
                   </td>
                   <td>
                    <?php 
                        //echo $row->requested_date;  
                        echo date("d-m-Y H:i:s", strtotime($row->requested_date));
                     ?>               
                   </td>
                   

                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>