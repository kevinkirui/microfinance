<?php include 'admin_header.php' ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url(); ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Guarantor Details</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                  
                  <th>Mobile Number</th>
                  <th>ID number</th>
                  
                  <th>Occupation</th>
                   <th>KRA Pin</th>
                  
                  
                  
                </tr>
              </thead>
              <tbody>
                <?php  
                foreach ($h->result() as $row)  
                {  
                 ?>
                <tr class="odd gradeX">
               
                 <td><?php echo $row->guarantor_fname; ?></td>
                 <td><?php echo $row->guarantor_mname;?></td>
                 <td><?php echo $row->guarantor_lname;?></td>
                 <td class="center"><?php echo $row->guarantor_phone;?></td>
                  <td class="center"><?php echo $row->guarantor_id_number;?></td>
                 <td class="center"><?php echo $row->guarantor_occupation;?></td>
                  <td class="center"><?php echo $row->guarantor_krapin;?></td>
               
               
                </tr>
                 <?php }  
                  ?>  
              </tbody>
            </table>
          </div>
        </div>
        
             
      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<?php include 'footer.php'; ?>