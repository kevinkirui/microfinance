<?php

 class Officermodel extends CI_Model  
       {  
          
     
          function __construct()  
          {  
             // Call the Model constructor  
             parent::__construct();  
             
          }  
          
          
           public function login($data) {

            //$condition = "admin_email =" . "'" . $data['admin_email'] . "' AND " . "admin_password =" . "'" . $data['admin_password'] . "'";
            $this->db->select('*');
            $this->db->from('loan_officer');
            $this->db->where('officer_email=',$data['client_email']);
            $this->db->limit(1);
            $query = $this->db->get();
             foreach($query->result_array() as $row)
            {
               
               $existingHashFromDb = $row['officer_password'];
               //$isPasswordCorrect = password_verify($data['admin_password'], $existingHashFromDb); 
               
               if(password_verify($data['client_password'], $existingHashFromDb))
               {
                   return true;
               }
               else
               {
                   return false;
               }
            }
           /*
            if ($query->num_rows() == 1) {
            return true;
            } else {
            return false;
            } */
            }
            
             public function read_user_information($client_email) 
            {

                $condition = "officer_email =" . "'" . $client_email . "'";
                $this->db->select('*');
                $this->db->from('loan_officer');
                $this->db->where($condition);
                $this->db->limit(1);
                $query = $this->db->get();

                if ($query->num_rows() == 1) {
                return $query->result();
                } else {
                return false;
                }
            }
            
            public function add_client($data)
            {
                $this->db->insert('customer', $data);
            }
            
             public function add_guarantor($gdata)
            {
                $this->db->insert('guarantor', $gdata);
            }
            
            public function add_request($data)
            {
                $this->db->insert('loan_request', $data);
            }
            
             public function update($phone)
            {
                $this->db->query("UPDATE customer SET loan_status=2 WHERE customer_phone='$phone'");
            }
            
             public function my_clients($email)
            {
                $this->db->select('*');
                $this->db->from('customer,loan_status');
                $this->db->where('customer.loan_status=loan_status.loan_id');
                //$this->db->or_where('customer.customer_id_number=loan_request.customer_id');

                $this->db->where('loan_officer_email',$email);
                 
                $query=$this->db->get();
                
                return $query;
            }
            
             public function level()
          {
             
             $this->db->select('*');
             $this->db->from('location');
             $query =$this->db->get();
             return $query;
           
          }
          
           public function get_products()
          {
             
             $this->db->select('*');
             $this->db->from('product');
             $query =$this->db->get();
             return $query;
           
          }
            
              public function clients_active($email)
            {
                $this->db->select('*');
                $this->db->from('customer,loan');
                 $this->db->where('loan.loan_status',3);
                 $this->db->where('customer.customer_id_number=loan.customer_id');
                $this->db->where('loan_officer_email',$email);
                 
                $query=$this->db->get();
                
                return $query;
            }
            
            public function clients_rejected($email)
            {
                $this->db->select('*');
                $this->db->from('customer,loan_rejected,loan_request');
                 $this->db->where('customer.loan_status',4);
                $this->db->where('loan_request.ref_no=loan_rejected.ref_no');
                $this->db->where('customer.customer_id_number=loan_rejected.customer_id');
                $this->db->where('customer.customer_id_number=loan_request.customer_id');


                $this->db->where('loan_officer_email',$email);
                 
                $query=$this->db->get();
                
                return $query;
            }
       }