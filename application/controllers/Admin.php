<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function __construct()
	{
		
		$this->load->helper('url');
                
             
                //$this->load->library("pagination");

                $this->load->database();

                // Load form helper library
                //$this->load->library('cart');

                // Load form validation library
                $this->load->library('form_validation');

                // Load session library
                $this->load->library('session');

                // Load database
                $this->load->model('Officermodel');
                
                $this->load->helper('form');

                

	}

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
         public function index()
	{

            $this->load->view('admin_login');
             
        }
        
        public function add_officer()
	{
	      //check sessions
	      
	       $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
	       
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $this->load->view('adminaddofficer',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
             
        }
        
         public function guarantor_details()
	{
	      //check if user is logged in
	        $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           //display reason form and pass the variables
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h']=$this->Adminmodel->guarantor_details($phone);
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           date_default_timezone_set('Africa/Nairobi');
           $now = date('Y-m-d H:i:s');
           $data['leo'] = $now;
         
            
           
           $this->load->view('admin_guarantor_details',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
         public function view_transfer()
	{
	      //check if user is logged in
	        $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           //display reason form and pass the variables
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           //get max received id
           $max_id=$this->Adminmodel->max_transfer_id($phone);
           
           //get details and populate view

           $data['h']=$this->Adminmodel->transfer_details($max_id);
           
            
         
            
           
           $this->load->view('view_transfer',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
         public function transfer_process()
	{
	      //check if user is logged in
	        $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           //display reason form and pass the variables
           $phone =  $this->input->post('phone');
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           //get max received id
           $max_id=$this->Adminmodel->max_transfer_id($phone);
           
           //calculate balance
            $last_paid =  $this->input->post('last_paid');
            
            $amount =  $this->input->post('amount');
            
            $balance= $last_paid-$amount;
            
            //update received
            
             $this->Adminmodel-> update_received($max_id,$balance);
             
             date_default_timezone_set('Africa/Nairobi');
              $now = date('Y-m-d H:i:s');
             
             //insert to registration fees
              $dara=array(
                       
                       'registration_phone' => $this->input->post('phone'),                    
                     
                       'registration_transaction_code' => 'Transfer',
                       'registration_amount' =>$amount,
                       'registration_date' =>$now,
                       
                       );
             
             $this->Adminmodel-> insert_registration($dara); 
           
           
           //get details and populate view

         
           
            
          $data['message']='Records transfer successful';
            
           
           $this->load->view('transfer_success',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
         public function all_officers()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h'] = $this->Adminmodel->get_officers();
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('all_officers',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
        
        public function all_admins()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h'] = $this->Adminmodel->get_admins();
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('all_admins',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
         public function change()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           //$data['h'] = $this->Adminmodel->get_officers();
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('admin_change',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
         public function change_process()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $email= $this->session->userdata['loggged_in']['admin_email'];
            //get the old password and check if it is correct
            $old_pass        = $this->input->post('old');
             $password        = $this->input->post('password');
            $pao=$this->hash_password($password);

           $this->load->database();
     
           $this->load->model('Adminmodel');
           

           $result = $this->Adminmodel->confam($old_pass,$email);
           
           if($result===true)
           {
                $this->load->library('form_validation');
               $this->form_validation->set_rules('password', 'Password', 'required');
               $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
               
               

                if ($this->form_validation->run() == FALSE)
                {
                        $data['disbursecount'] = $this->Adminmodel->disburse_count();
                        $data['requestcount'] = $this->Adminmodel-> request_count();
                        $this->load->view('admin_change',$data);
                }
                else
                {
                        //update password
                        $dasa=array(
                            
                               'admin_password'=>$pao,
                            );
                         $data['messo'] = "password update successfully";
                        $this->Adminmodel->update_pass($email,$dasa);
                        $data['disbursecount'] = $this->Adminmodel->disburse_count();
                        $data['requestcount'] = $this->Adminmodel-> request_count();
                        $this->load->view('admin_change',$data);
                }
               
               
           }
           
           else
           {
               
           $data['message']="Your old password is wrong";    
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('admin_change',$data);
               
           }
           
           //confirm new passwords
           
          
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
          public function approve_process()
	{
           $idnumber = $this->uri->segment(3);
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $max_id=$this->Adminmodel->get_max_id($idnumber);
           
           
           //update loan_request status
           $this->Adminmodel->update_request($idnumber,$max_id);

            //update loan_request status
           $this->Adminmodel->update_approved($idnumber,$max_id);

           /*
           //change status
           $this->load->helper('date');
           
   
              
           
            
             $this->load->database();

             $this->load->model('Adminmodel');
            //insert into database
            
            //$result=$this->Adminmodel->get_officer_id($idnumber);
              date_default_timezone_set('Africa/Nairobi');
              $now = date('Y-m-d H:i:s');
                $dara=array(
                       
                       'loan_name' => "Basic",                    
                       'customer_id' => $idnumber,
                       'principal' => 5000,
                       'loan_status' =>3,
                       'start_date' =>$now,
                       
                       );
                      
            
             $this->Adminmodel->add_successful($dara);
             */
             
             $this->load->database();
     
             $this->load->model('Adminmodel');

             $this->Adminmodel->approve_process($idnumber);
            
            redirect('admin/approve');
             
            
             
        }
        /*
        public function reject_reason()
	{
           //display reason form and pass the variables
           $idnumber =  $this->uri->segment(3);
           $data['id']=$idnumber;
           //$this->load->database();
     
           //$this->load->model('Adminmodel');

           //$this->Adminmodel->approve_reject($idnumber);
           
           $this->load->view('admin_reason',$data);
             
        }
         * /
         */
        //disburse process
        
        public function registration_fees()
    {
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $data['from'] = $from;
            $data['to'] = $to;

           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
          

           $data['h']=$this->Adminmodel->get_fees($from,$to);
           
           $this->load->view('admin_fees',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
             
        }
        
         public function mpesa_received()
    {
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $data['from'] = $from;
            $data['to'] = $to;

           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
          

           $data['h']=$this->Adminmodel->get_received($from,$to);
           
           $this->load->view('admin_received',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
             
        }
        
         public function sms_list()
    {
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $data['from'] = $from;
            $data['to'] = $to;

           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
          

           $data['h']=$this->Adminmodel->sms_list($from,$to);
           
           $this->load->view('sms_list',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
             
        }
        
         public function mpesa_received_export()
    {
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){
               $from='2017-06-29 00:00';
              $to='2017-06-29 23:59';
             
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $data['from'] = $from;
            $data['to'] = $to;

           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
          

           $data['h']=$this->Adminmodel->get_received($from,$to);
           
           $this->load->view('admin_received_export',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
             
        }
        
        public function disburse_process()
	{
           //$id = $this->uri->segment(3);
           
          $phone=$this->input->post('phone');
          $refno=$this->input->post('mpesa_code');
           
           //change status
           $this->load->helper('date');
           
   
              
           
            
             $this->load->database();

             $this->load->model('Adminmodel');
            //insert into database
            
              
             //get principal and product from loan requests, remember to implement for multiple instances
             //get maximum_id
             
              $max_id=$this->Adminmodel->get_max_id_disburse($phone);

              $amount=$this->Adminmodel->get_pesa($phone,$max_id);
              
              $fname=$this->Adminmodel->get_fname($phone,$max_id);
              
                $startdate=$this->Adminmodel->get_start($phone,$max_id);
               
              
              
                $prid=$this->Adminmodel->get_time($phone,$max_id);
                
                /*
                if($prid==1)
                {
                     $due=  date_add($startdate, date_interval_create_from_date_string('28 days'));
                }
                if($prid==2)
                {
                     $due=  date_add($startdate, date_interval_create_from_date_string('36 days'));
                }
                if($prid==3)
                {
                     $due=  date_add($startdate, date_interval_create_from_date_string('56 days'));
                }
                
                */
                
                 $tarehe = date("d-m-Y h:i:s");
                            // $min_seconds = date("h:i:s");

                             switch ($prid) {
                                case '1':
                                    $expiry_date = date('d-m-Y', strtotime($startdate.' +28 day'));
                                    break;
                                case '2':
                                    $expiry_date = date('d-m-Y', strtotime($startdate.' +42 day'));
                                    break;
                                case '3':
                                     $expiry_date = date('d-m-Y', strtotime($startdate.' +56 day'));
                                    break;                                 
                                 default:
                                      $expiry_date = date('d-m-Y', strtotime($startdate.' +0 day'));
                                 break;
                             }
             
             //$principal=$this->Adminmodel->get_principal($idnumber);
             
             //add data to loans table
             
              date_default_timezone_set('Africa/Nairobi');
              $now = date('Y-m-d H:i:s');
                $dara=array(
                       
                       'loan_name' => "Product",                    
                       'customer_phone' => $phone,
                       'principal' => $amount,
                       'loan_status' =>6,
                       'start_date' =>$now,
                       
                       );
             $this->load->database();

             $this->load->model('Adminmodel');        
            
             $this->Adminmodel->add_successful($dara);
             
             //get data from mpesa disburse paybill and post to mpesa send table
              date_default_timezone_set('Africa/Nairobi');
              $saa = date('Y-m-d H:i:s');
              //mpesa code
              
                //get client phone
                $this->load->database();

                $this->load->model('Adminmodel');        
            
                //$phone=$this->Adminmodel->get_client_phone($id);
                $totalamount=$amount*1.2;
                
                $dasa=array(
                       
                       'mpesa_amount' => $amount,                    
                       'mpesa_code' => $refno,
                       'mpesa_type' => 'D',
                       'mpesa_phone' =>$phone,
                       'mpesa_date' =>$saa,
                       
                       );
           
             $message="Hi $fname, We've sent you a total of $amount ksh from Rafric via M-Pesa. Your repayment of $totalamount is due to PayBill 742254 by $expiry_date Regards.";
             
             /*
               
                require_once('AfricasTalkingGateway.php');    
                
                $username    = "254sms";
                $apiKey      = "ffa4f6937be49a50e56533b549a9c13e5eed8e3fae362a210e881312b1219486";
                $from        = "254sms";
                $recipients = $phone;
                $message =$message;
                $gateway  = new AfricaStalkingGateway($username, $apiKey);
                $results  = $gateway->sendMessage($recipients, $message, $from);
                
                            if ( count($results) ) {
                              foreach($results as $result) {
                                  $sNumber = $result->number;
                                  $sMessageId = $result->messageId;
                                  $sStatus = $result->status;
                              }
                               }
                           else {
                          $errormessage = $gateway->getErrorMessage();
                          echo $errormessage;
                
                          }
       

             end africastalking
             */
             
             
            $username = "rafric";
            $password = "rafric2017";
            
            //$phone = "254728369514";
            //$message = "forth Test SMS on Curl";
            
            $smsdata = array(
             "username" => $username,
             "password" => $password,
             "phone" => $phone,
             "message" => $message
            );
            
            
            $smsdata_string = json_encode($smsdata);
            $api_url = "http://api.qbettech.com/sendsms.php"; //plain HTTP
            
            
            
            $ch = curl_init($api_url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $smsdata_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
             'Content-Type: application/json',
             'Content-Length: ' . strlen($smsdata_string))
            );
            $apiresult = curl_exec($ch);
            
            if (!$apiresult) {
             die("ERROR on URL[$urls] | error[" . curl_error($ch) . "] | error code[" . curl_errno($ch) . "]\n");
            }
            curl_close($ch);
            
            $myArray = explode(';', $apiresult);
            $balance = $myArray[0];
            $msgid = $myArray[1];
            $status = $myArray[2];
            //$phone = $myArray[3];
            $state = $myArray[4];
            
            $sentsms=array(
                
                       'system_id' => $msgid,                    
                       'phone' => $phone,                    
                       'message' => $message,
                       'status' => $status,
                       'balance' =>$balance,
                       'date' =>$now,
                       
                       );
            
             $this->load->database();

             $this->load->model('Adminmodel');        
            
             $this->Adminmodel->add_message($sentsms);
             
             
            
             $this->load->database();

             $this->load->model('Adminmodel');        
            
             $this->Adminmodel->add_mpesa_table($dasa);
             
             //change mpesa code status in loan request, it helps us to get disbursed loans
             
             $this->load->database();

             $this->load->model('Adminmodel');        
            
             $this->Adminmodel->update_mpesacode($max_id,$refno);
             
             //add null to mpesa received
              $kasa=array(
                       
                       'received_amount' => 0,                    
                       'received_transaction_id' => 'DUMMY',
                       'check_status' => 1,
                       'received_phone' =>$phone,
                       'received_date' =>$saa,
                       
                       );
              
             $this->load->database();

             $this->load->model('Adminmodel');        
            
             $this->Adminmodel->add_dummy($kasa);
             
             //change status to disbursed
             
             
             $this->load->database();
     
             $this->load->model('Adminmodel');

             $this->Adminmodel->disburse_change($phone);
            
             redirect('admin/disburse');
             
            
             
        }
        
        public function reject_reason()
	{
           //display reason form and pass the variables
           $idnumber =  $this->uri->segment(3);
           $data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['disbursecount'] = $this->Adminmodel->disburse_count();

           //$this->Adminmodel->approve_reject($idnumber);
           
           $this->load->view('admin_reason',$data);
             
        }
        
         public function paymentschedule()
	{
           //display reason form and pass the variables
           //add session
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h']=$this->Adminmodel->paymentschedule($phone);
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           date_default_timezone_set('Africa/Nairobi');
           $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;
         
            
           
           $this->load->view('payment_schedule',$data);
           }
           else
           {
               
               $this->load->view('admin_login');
           
           }
             
        }
        
        public function deleteloanofficer()
	{
           //display reason form and pass the variables
           //add session
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           
            $data['message'] = "Loan officer has been deleted successfully";
            
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
            //change status of customer
                 $this->load->database();

                 $this->load->model('Adminmodel');
            
                 $this->Adminmodel->deleteofficer($phone);
                 
                 $data['h'] = $this->Adminmodel->get_officers();
           
                $data['disbursecount'] = $this->Adminmodel->disburse_count();
                $data['requestcount'] = $this->Adminmodel-> request_count();
               
                $this->load->view('all_officers',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
         }
         
         public function deleteadmin()
	{
           //display reason form and pass the variables
           //add session
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           
            $data['message'] = "Admin has been deleted successfully";
            
           $ema=  $this->uri->segment(3);
           
           $email=base64_decode($ema);
           //$data['id']=$idnumber;
            //change status of customer
                 $this->load->database();

                 $this->load->model('Adminmodel');
            
                 $this->Adminmodel->deleteadmin($email);
                 
                 $data['h'] = $this->Adminmodel->get_admins();
           
                $data['disbursecount'] = $this->Adminmodel->disburse_count();
                $data['requestcount'] = $this->Adminmodel-> request_count();
               
                $this->load->view('all_admins',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
         }
        
          public function clearloans()
	{
           //display reason form and pass the variables
           //add session
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
            //change status of customer
                 $this->load->database();

                 $this->load->model('Adminmodel');
            
                 $this->Adminmodel->updatecustomer($phone);
                 
                 
                 //change statu of loan request
                 
                 $this->load->database();

                 $this->load->model('Adminmodel');
                 
                 //get max id 
            
                 $id=$this->Adminmodel->get_max_id_complete($phone);
                 //update
                  $this->Adminmodel->updateloanrequest_special($id);
                $data['message']='The loan has been cleared successfully for the customer';
                 $data['disbursecount'] = $this->Adminmodel->disburse_count();
                 $data['requestcount'] = $this->Adminmodel-> request_count();
            
           //call loan balance here
               $this->load->view('clear_success',$data);
           
          
           }
           else
           {
               
               $this->load->view('admin_login');
           
           }
             
        }
        
        
        public function view_statement()
	{
           //display reason form and pass the variables
           //check sessions
           $this->load->library('session');
             
        
               
              if($this->session->userdata('loggged_in')){
               
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           $phone=  $this->uri->segment(3);
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                $data['phone']=$phone;
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('mpesa_repayments',$data);
               
           }
           //$this->load->database();
     
           //$this->load->model('Adminmodel');

           //$this->Adminmodel->approve_reject($idnumber);
           else
           {
               
              $data['phone']=$phone;
               $this->load->view('mpesa_repayments',$data);
           
           
           }
           
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
        public function view_statement_export()
	{
           //display reason form and pass the variables
           //check sessions
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           $phone=  $this->uri->segment(3);
           
             $from='2017-06-29 00:00';
              $to='2017-06-29 23:59';
             
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  
                $to= $this->uri->segment(4);
                  
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('mpesa_repayments_export',$data);
               
           }
           //$this->load->database();
     
           //$this->load->model('Adminmodel');

           //$this->Adminmodel->approve_reject($idnumber);
           else
           {
               
           
           $this->load->view('mpesa_repayments_export',$data);
           
           
           }
           
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
        
          public function approve_reject()
	{
           //add reason and amount to rejected
           //get reason
           $reason = $this->input->post('reason');
           $id = $this->input->post('id');
           
           //get amount
           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $max_id=$this->Adminmodel->get_max_id($id);

           $amount=$this->Adminmodel->get_amount($id,$max_id);
           $reference=$this->Adminmodel->get_reference($id,$max_id);

          // print_r($mat);
           //$amount=$mat['request_amount'];
           date_default_timezone_set('Africa/Nairobi');
           $now = date('Y-m-d H:i:s');
           $data=array(
               'rejected_reason'=>$reason,
               'customer_id'=>$id,
               'rejected_amount'=>$amount,
               'rejected_time'=>$now,
               'ref_no'=>$reference,
           );
           //insert into rejected table
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $this->Adminmodel->insert_rejected($data);

           //change status to rejected
           $idnumber =  $id;

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $this->Adminmodel->approve_reject($idnumber);
           
           //Change status of requested loan
           
           //get maximum id for the id number
           
           
           //update 
           $this->Adminmodel->update_request($id,$max_id);
           
           $this->approve();
             
        }
        
         public function dashboard()
	{
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
           
           $this->load->database();
     
           $this->load->model('Adminmodel');

           //$data['h'] = $this->Adminmodel->summary();
           $data['customer_count'] = $this->Adminmodel->count_customers();
           $data['a'] = $this->Adminmodel->truestatus_dashboard($from,$to);
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['pri'] = $this->Adminmodel-> truestatus_principal();
           $data['rec'] = $this->Adminmodel-> total_rec();
           //current time
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;
            $data['h'] = $this->Adminmodel->truestatus($from,$to);
           $data['k'] = $this->Adminmodel->truestatuskev();
           
           $this->load->view('admin_dashboard',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
         public function admin_balance()
	{
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

           // $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
            
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           

           $data['from'] = $from;
            $data['to'] = $to;
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           //count
           $data['regionthree'] = $this->Adminmodel->regtatubalcount($from,$to);
           $data['kimumucount'] = $this->Adminmodel->kimumtolscount($from,$to);
            $data['mailicount'] = $this->Adminmodel->mailitolscount($from,$to);
           
           
           //get total amounts received 
           $data['regtatuz'] = $this->Adminmodel->regtatubal($from,$to);
           $data['kimumtols'] = $this->Adminmodel->kimumtols($from,$to);
            $data['mailitols'] = $this->Adminmodel->mailitols($from,$to);
           
           //get individual records
           $data['regtatu'] = $this->Adminmodel->regthreebal($from,$to);
           $data['kimbal'] = $this->Adminmodel->kimumubal($from,$to);
           $data['mailbal'] = $this->Adminmodel->mailibal($from,$to);
           
           
           
           $this->load->view('admin_balance',$data);
           
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
          public function admin_balance_export()
	{
            $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

           // $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            //get date filter values
             //$fro= $this->input->post('from');
             // $t= $this->input->post('to');
             // $from = date("Y-m-d 00:00", strtotime($fro));
             // $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
            
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           

           //$data['h'] = $this->Adminmodel->summary();
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           //count
           $data['regionthree'] = $this->Adminmodel->regtatubalcount($from,$to);
           $data['kimumucount'] = $this->Adminmodel->kimumtolscount($from,$to);
            $data['mailicount'] = $this->Adminmodel->mailitolscount($from,$to);
           
           
           //get total amounts received 
           $data['regtatuz'] = $this->Adminmodel->regtatubal($from,$to);
           $data['kimumtols'] = $this->Adminmodel->kimumtols($from,$to);
            $data['mailitols'] = $this->Adminmodel->mailitols($from,$to);
           
           //get individual records
           $data['regtatu'] = $this->Adminmodel->regthreebal($from,$to);
           $data['kimbal'] = $this->Adminmodel->kimumubal($from,$to);
           $data['mailbal'] = $this->Adminmodel->mailibal($from,$to);
           
           
           
           $this->load->view('admin_balance_export',$data);
           
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
        public function activeexport()
        {
              $this->load->library('session');
           if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            // get date filter data
             // $fro= $this->input->post('from');
             // $t= $this->input->post('to');
              //$from = date("Y-m-d 00:00", strtotime($fro));
             // $to = date("Y-m-d 23:59", strtotime($t));
            //  $from= $this->uri->segment(3);
            //  $to= $this->uri->segment(4);
              
              $from='2017-06-29 00:00';
              $to='2017-06-29 23:59';
             
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
          

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           
            $this->load->view('admin_active_export',$data);

             
           }
            
           else
           {
                $this->load->view('admin_login'); 
           }
             
        }
        
        public function registration_fees_export()
        {
              $this->load->library('session');
           if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            // get date filter data
             // $fro= $this->input->post('from');
             // $t= $this->input->post('to');
              //$from = date("Y-m-d 00:00", strtotime($fro));
             // $to = date("Y-m-d 23:59", strtotime($t));
            //  $from= $this->uri->segment(3);
            //  $to= $this->uri->segment(4);
              
              $from='2017-06-29 00:00';
              $to='2017-06-29 23:59';
             
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
          

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->get_fees($from,$to);
           
            $this->load->view('admin_fees_export',$data);

             
           }
            
           else
           {
                $this->load->view('admin_login'); 
           }
             
        }
        
        public function approve()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
                       


           $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['requestcount'] = $this->Adminmodel-> request_count();
               $data['disbursecount'] = $this->Adminmodel->disburse_count();

           $result = $this->Adminmodel->display_approve();
           if($result!=FALSE)
           {
           
               $data['h'] = $this->Adminmodel->display_approve();
              

               $this->load->view('approve_clients',$data);
               
           }
           else
           {
                 $data['message'] = "Sorry! There are no records at the moment";
                
                 $this->load->view('approve_clients',$data);
               
           }
           }
            
           else
           {
                $this->load->view('admin_login'); 
           }
             
        }
        
        public function pending_approval()
	{
           $this->load->library('session');
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $result = $this->Adminmodel->display_approve();
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {
           
               $data['h'] = $this->Adminmodel->display_approve();

               $this->load->view('approve_pending',$data);
               
           }
           else
           {
                 $data['message'] = "Sorry! There are no records at the moment";
                
                 $this->load->view('approve_pending',$data);
               
           }
           
          
             
        }
        
         public function disburse()
	{
           $this->load->library('session');
             if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $result = $this->Adminmodel->display_disburse();
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           
           if($result!=FALSE)
           {


           $data['h'] = $this->Adminmodel->display_disburse();
           
           $this->load->view('disburse',$data);
           
           }
           else
           {
               $data['message'] = "Sorry! There are no records at the moment";
                
               $this->load->view('disburse',$data);
               
           }
         } 
         else
         {
              $this->load->view('admin_login'); 
         }
           //$this->load->view('disburse',$data);
             
        }
        
         public function view_bankstatement()
	{
           //display reason form and pass the variables
           //check sessions
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){
               
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           $phone=  $this->uri->segment(3);
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['phone'] = $phone;

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('admin_mpesa_statement',$data);
               
           }
          
           else
           {
               
           
           $this->load->view('admin_mpesa_statement',$data);
           
           
           }
         }
         else
         {
             $this->load->view('admin_login');
         }
           
             
        }
        
          public function view_bankstatement_export()
	{
           //display reason form and pass the variables
           //check sessions
           $this->load->library('session');
             
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           $phone=  $this->uri->segment(3);
           
            $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('admin_mpesa_statement_export',$data);
               
           }
          
           else
           {
               
           
           $this->load->view('admin_mpesa_statement_export',$data);
           
           
           }
         }
         else
         {
             $this->load->view('admin_login');
         }
           
             
        }
         public function awaiting_disbursement()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $result= $this->Adminmodel->display_awaiting_disburse();
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {


           $data['h'] = $this->Adminmodel->display_awaiting_disburse();
           
           $this->load->view('admin_awaiting_disbursement',$data);
           
           }
           else
           {
               $data['message'] = "Sorry! There are no records at the moment";
                
               $this->load->view('admin_awaiting_disbursement',$data);
               
           }
         }
         else
         {
           $this->load->view('admin_login'); 

         }
             
        }
        
         public function toExcel()
  {
    $this->load->view('spreadsheet_view');
  }
        
         public function activeloans()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            // get date filter data
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
           
           //add from and to to be used by export
          
     
           $this->load->model('Adminmodel');
            $data['from'] = $from;
           $data['to'] = $to;
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           
            $this->load->view('admin_active',$data);
           }
           else
           {
                $this->load->view('admin_login'); 
           }
             
        }
        
        public function truestatus()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){
               
           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
              //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
           $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['from'] = $from;
           $data['to'] = $to;
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('true_status',$data);
 
   
            }else{
                 $this->load->view('admin_login');
            }
                       
        }
        
         public function truestatus_export()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){
               
           $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
              //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
              if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('true_status_export',$data);
 
   
            }else{
                 $this->load->view('admin_login');
            }
                       
        }
        
        public function loans_duetoday()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           $data['k'] = $this->Adminmodel->truestatuskev();
          //  $data['g'] = $this->Adminmodel->sum_received();
            $data['leo'] =  $now;
            $this->load->view('admin_duetoday',$data);
            }
            else
            {
                $this->load->view('admin_login'); 
            }
        }
        
        
        public function loans_duetoday_export()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){
            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

             

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           $data['k'] = $this->Adminmodel->truestatuskev();
          //  $data['g'] = $this->Adminmodel->sum_received();
            $data['leo'] =  $now;
            $this->load->view('admin_duetoday_export',$data);
            }
            else
            {
                $this->load->view('admin_login'); 
            }
        }
        
    public function get_arrears()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
              $emai=$this->uri->segment(3);
               $email    =urldecode($emai);
              $name=$this->uri->segment(4);
            //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['from'] = $from;
            $data['to'] = $to;
            //get arrears based on officer, will automate later or after I get hold of a good pay cheque
          
           $data['h'] = $this->Adminmodel->truestatus_my($from,$to,$email);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
           $data['name'] = $name;
           $data['emai'] =   $emai;
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('admin_myarrears',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
            public function my_arrears_export()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
              $emai=$this->uri->segment(3);
               $email    =urldecode($emai);
              $name=$this->uri->segment(4);
            //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
              if($this->uri->segment(5) OR $this->uri->segment(6))
             {
                $from= $this->uri->segment(5);
                  $from = base64_decode($from);
                $to= $this->uri->segment(6);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }


           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['from'] = $from;
            $data['to'] = $to;
            //get arrears based on officer, will automate later or after I get hold of a good pay cheque
          
           $data['h'] = $this->Adminmodel->truestatus_my($from,$to,$email);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
           $data['name'] = $name;
           $data['emai'] =   $emai;
           
            $this->load->view('admin_myexport',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
        public function loan_arrears()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['from'] = $from;
            $data['to'] = $to;
            //get arrears based on officer, will automate later or after I get hold of a good pay cheque
           $adilaemail='adilachelimo@gmail.com';
           $abrahamemail='abrahamkorir85@gmail.com';
           $spenceremail='ngenospencer90@gmail.com';
            $data['h'] = $this->Adminmodel->truestatus($from,$to);
           $data['adila'] = $this->Adminmodel->truestatus_my($from,$to,$adilaemail);
           $data['abu'] = $this->Adminmodel->truestatus_my($from,$to,$abrahamemail);
           $data['spencer'] = $this->Adminmodel->truestatus_my($from,$to,$spenceremail);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('admin_arrears',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
        
          public function loan_arrears_export()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            
            //get date filter values
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }


           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('admin_arrears_export',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
        public function all_customers()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_customers();
           
            $this->load->view('admin_allcustomers',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
        public function all_guarantors()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_guarantors();
           
            $this->load->view('admin_guarantors',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
        public function mpesa_repayments()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_customers();
           
            $this->load->view('repayments_view',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
         public function edit_guarantor()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $guarantorid= $this->uri->segment(3);

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->get_guarantor($guarantorid);
           
            $this->load->view('edit_guarantor',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
        
         public function edit_customer()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $customerid= $this->uri->segment(3);

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->get_customer($customerid);
           
            $this->load->view('edit_customer',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
        //transfer customer to another loan officer
        
         public function transfer_customer()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $customerid= $this->uri->segment(3);

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->get_customer($customerid);
           
           $data['l'] = $this->Adminmodel->get_offi();

           
           //get customer details and transfer to loan officer in a dropdown
            $this->load->view('transfer_customer',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
        public function edit_customer_process()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $customerid= $this->input->post('customer_id');

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           $dara= array(
                 
                       
                                           
                       'customer_fname' => $this->input->post('customer_fname'),
                       'customer_middlename' => $this->input->post('customer_mname'),
                       'customer_lname' => $this->input->post('customer_lname'),
                       'customer_id_number' => $this->input->post('customer_id_number'),
                       'customer_phone' => $this->input->post('customer_phone'),
                       'customer_email' => $this->input->post('customer_email'),
                       'customer_occupation' => $this->input->post('customer_occupation'),
                      // 'order_custid' => $clientid,
                       
                        );

        $this->Adminmodel->edit_customer($customerid, $dara);
           
           $data['message'] ="customer record updated successfully";
           
            $data['h'] = $this->Adminmodel->get_customer($customerid);
           
            $this->load->view('edit_customer',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
         public function transfer_customer_process()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $customerid= $this->input->post('customer_id');

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           $dara= array(
                 
                       
                                           
                       'loan_officer_email' => $this->input->post('officer_email'),
                       
                       
                        );

        $this->Adminmodel->edit_transfer($customerid, $dara);
           
           $data['message'] ="customer assigned to loan officer successfully";
           
                       $data['h'] = $this->Adminmodel->all_customers();

           
            $this->load->view('admin_allcustomers',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
         public function edit_guarantor_process()
	{
           $this->load->library('session');
            if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
                           
            $guarantorid= $this->input->post('guarantor_id');

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           $dara= array(
                 
                       
                                           
                       'guarantor_fname' => $this->input->post('guarantor_fname'),
                       'guarantor_mname' => $this->input->post('guarantor_mname'),
                       'guarantor_lname' => $this->input->post('guarantor_lname'),
                       'guarantor_id_number' => $this->input->post('guarantor_id_number'),
                       'guarantor_phone' => $this->input->post('guarantor_phone'),
                       'guarantor_occupation' => $this->input->post('guarantor_occupation'),
                     
                       
                        );

           $this->Adminmodel->edit_guarantor($guarantorid, $dara);
           
           $data['message'] ="Guarantor record updated successfully";
           
            $data['h'] = $this->Adminmodel->get_guarantor($guarantorid);
           
            $this->load->view('edit_guarantor',$data);
            }
            else
            {
                $this->load->view('admin_login');
            }
        }
        
         public function all_disbursed()
         
	{
            $this->load->library('session');
           if($this->session->userdata('loggged_in')){
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['from'] = $from;
           $data['to'] = $to;
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           //count
            $data['kimumucount'] = $this->Adminmodel->kimumucount($from,$to);
             $data['regionthree'] = $this->Adminmodel->regionthreecount($from,$to);
             $data['mailicount'] = $this->Adminmodel->mailicount($from,$to);
          //sum
             $data['mailisum'] = $this->Adminmodel->mailisum($from,$to);
             $data['regionthreesum'] = $this->Adminmodel->regionthreesum($from,$to);
              $data['kimumusum'] = $this->Adminmodel->kimumusum($from,$to);

           $data['kimumu'] = $this->Adminmodel->all_disbursed_kimumu($from,$to);
           $data['maili'] = $this->Adminmodel->all_disbursed_maili($from,$to);
            $data['three'] = $this->Adminmodel->all_disbursed_three($from,$to);
           $this->load->view('admin_disbursed',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        
             
        }
        
        public function disbursed_officer()
         
	{
            $this->load->library('session');
           if($this->session->userdata('loggged_in')){
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['from'] = $from;
           $data['to'] = $to;
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           //count
            $data['kimumucount'] = $this->Adminmodel->spencercount($from,$to);
             $data['regionthree'] = $this->Adminmodel->abucount($from,$to);
             $data['mailicount'] = $this->Adminmodel->adilacount($from,$to);
          //sum
              $data['kimumusum'] = $this->Adminmodel->spencersum($from,$to);
              $data['regionthreesum'] = $this->Adminmodel->abusum($from,$to);
             $data['mailisum'] = $this->Adminmodel->adilasum($from,$to);
             
             

           $data['kimumu'] = $this->Adminmodel->all_disbursed_spencer($from,$to);
            $data['three'] = $this->Adminmodel->all_disbursed_abu($from,$to);
           $data['maili'] = $this->Adminmodel->all_disbursed_adila($from,$to);
           
           $this->load->view('admin_disbursed_officer',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        
             
        }
        
        public function disbursed_officer_export()
         
	{
            $this->load->library('session');
           if($this->session->userdata('loggged_in')){
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
             // $from = date("Y-m-d 00:00", strtotime($fro));
            //  $to = date("Y-m-d 23:59", strtotime($t));
              
            
              if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['from'] = $from;
           $data['to'] = $to;
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           //count
            $data['kimumucount'] = $this->Adminmodel->spencercount($from,$to);
             $data['regionthree'] = $this->Adminmodel->abucount($from,$to);
             $data['mailicount'] = $this->Adminmodel->adilacount($from,$to);
          //sum
              $data['kimumusum'] = $this->Adminmodel->spencersum($from,$to);
              $data['regionthreesum'] = $this->Adminmodel->abusum($from,$to);
             $data['mailisum'] = $this->Adminmodel->adilasum($from,$to);
             
             

           $data['kimumu'] = $this->Adminmodel->all_disbursed_spencer($from,$to);
            $data['three'] = $this->Adminmodel->all_disbursed_abu($from,$to);
           $data['maili'] = $this->Adminmodel->all_disbursed_adila($from,$to);
           
           $this->load->view('admin_disbursed_officer_export',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        
             
        }
        
         public function all_disbursed_export()
         
	{
            $this->load->library('session');
           if($this->session->userdata('loggged_in')){
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
             // $from = date("Y-m-d 00:00", strtotime($fro));
            //  $to = date("Y-m-d 23:59", strtotime($t));
              
            
              if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           //count
            $data['kimumucount'] = $this->Adminmodel->kimumucount($from,$to);
             $data['regionthree'] = $this->Adminmodel->regionthreecount($from,$to);
             $data['mailicount'] = $this->Adminmodel->mailicount($from,$to);
          //sum
             $data['mailisum'] = $this->Adminmodel->mailisum($from,$to);
             $data['regionthreesum'] = $this->Adminmodel->regionthreesum($from,$to);
              $data['kimumusum'] = $this->Adminmodel->kimumusum($from,$to);

           $data['kimumu'] = $this->Adminmodel->all_disbursed_kimumu($from,$to);
           $data['maili'] = $this->Adminmodel->all_disbursed_maili($from,$to);
            $data['three'] = $this->Adminmodel->all_disbursed_three($from,$to);
           $this->load->view('admin_disbursed_export',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        
             
        }
        
        public function all_loans()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
               

           $this->load->database();
     
           $this->load->model('Adminmodel');
            $data['from'] = $from;
             $data['to'] = $to;
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_loans($from,$to);
           
            $this->load->view('admin_allloans',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
         public function all_loans_export()
	{
           $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
            // $fro= $this->input->post('from');
           //   $t= $this->input->post('to');
            //  $from = date("Y-m-d 00:00", strtotime($fro));
            //  $to = date("Y-m-d 23:59", strtotime($t));
              
            
              if($this->uri->segment(3) OR $this->uri->segment(4))
             {
                $from= $this->uri->segment(3);
                  $from = base64_decode($from);
                $to= $this->uri->segment(4);
                   $to = base64_decode($to);
               // echo $from;
             }
            else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
               

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_loans($from,$to);
           
            $this->load->view('admin_allloans_export',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }
        
        public function receive_money()
        {
            $this->load->database();
            $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $this->load->view('receive_money',$data);
        }
        
         public function add_admin()
        {
             $this->load->library('session');
           if($this->session->userdata('loggged_in')){
              $data['jina'] = $this->session->userdata['loggged_in']['admin_name']; 
               
            $this->load->database();
            $this->load->model('Adminmodel');
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
            $data['requestcount'] = $this->Adminmodel-> request_count();
            $this->load->view('add_admin',$data);
           }
           else
           {
               $this->load->view('admin_login');
           }
        }
        
        private function hash_password($password){
             return password_hash($password, PASSWORD_BCRYPT);
        }
        
        public function registerUser(){
          $this->load->library('session');
           if($this->session->userdata('loggged_in')){
          
               
          $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];   
           $fname =$this->input->post('admin_fname');
            $email =$this->input->post('admin_email');
            $password =$this->input->post('admin_password');
           $dara = array(
             'admin_fname' => $fname,
              'admin_email' => $email,
             'admin_password' => $this->hash_password($password)
        );
           
           $this->load->database();
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['requestcount'] = $this->Adminmodel-> insert_admin($dara);
           $data['success'] = "admin added successfully";
           $this->load->view('add_admin',$data);
           }
           else
           {
                $this->load->view('admin_login');
           }
           
           
        
        }
        
        public function summary()
	{
            $this->load->library('session');
            $ema = $this->session->userdata['loggged_in']['admin_email'];
            
           if(isset($ema))
           {
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['jina'] = $this->Adminmodel->get_admin_name($ema);
           } 

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h'] = $this->Adminmodel->summary();
           
            $this->load->view('admin_summary',$data);
             
        }
        
         public function rejected()
	{
             $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $result = $this->Adminmodel->rejected();
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {
              $data['h'] = $this->Adminmodel->rejected();
           
              $this->load->view('admin_rejected',$data);
            
           }
           else
           {
                 $data['message'] = "Sorry! There are no records at the moment";
                
                 $this->load->view('admin_rejected',$data);

               
           }
         }
         else
         {
             $this->load->view('admin_login');
         }
             
        }
        
        public function logout() {

// Removing session data
        $sess_array = array(
        'admin_name' => ''
        );
         $this->load->library('session');
        $this->session->unset_userdata('loggged_in', $sess_array);
        $data['message_display'] = 'Successfully Logged out';
        $this->load->view('admin_login', $data);
}
        
         public function clients()
	{
            $this->load->database();  
             //load the model  
            $this->load->model('Adminmodel');  
            
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
             
            $app =  $this->uri->segment(3);
            $str=base64_decode($app);
             //load the method of model  
            $data['h']=$this->Adminmodel->clients($str);  
             
            $this->load->view('officer_clients',$data);

        }
        public function user_login_process()
       {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('admin_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('admin_password', 'Password', 'trim|required');

  if ($this->form_validation->run() == FALSE) 
    {
    
       if(isset($this->session->userdata['loggged_in']))
           
         {
    
              $this->dashboard();
     
         }
     else
      {
     
             $this->load->view('admin_login');
      
      }
   } 
else 
   {
      // $pass =$this->input->post('admin_password');
                   //  $pao=$this->hash_password($pass);
        //$isPasswordCorrect = password_verify($password, $existingHashFromDb);
        $data = array(
        'admin_email' => $this->input->post('admin_email'),
        'admin_password' => $this->input->post('admin_password')
);

     $this->load->database();
     
     $this->load->model('Adminmodel');

    $result = $this->Adminmodel->login($data);

if ($result == TRUE) 
    
  {

    $admin_email = $this->input->post('admin_email');
    $result = $this->Adminmodel->read_user_information($admin_email);
    if ($result != false) 
       {
        $session_data = array(
        'admin_name' => $result[0]->admin_fname,
        'admin_email' => $result[0]->admin_email,
         );
    // Add user data in session
     $this->load->library('session');

        $this->session->set_userdata('loggged_in', $session_data);
                // $this->load->database();
     
               //$this->load->model('Adminmodel');

              //$data['orders'] = $this->Adminmodel->get_orders();
               
              $this->dashboard();
    }
 } 
else 
{
    
$data['error_message'] ='Invalid Username or Password';
$this->load->view('admin_login', $data);
    }
}
}
        
   public function add_officer_process()
	{
	       $this->load->library('session');
           if($this->session->userdata('loggged_in')){

            $data['jina'] = $this->session->userdata['loggged_in']['admin_name'];
             $password=1234;
             $data= array(
                 
                       
                                           
                       'officer_fname' => $this->input->post('customer_fname'),
                        'officer_middlename' => $this->input->post('customer_mname'),
                       'officer_lname' => $this->input->post('customer_lname'),
                       'officer_nationalid' => $this->input->post('customer_id'),
                       'officer_phone' => $this->input->post('customer_phone'),
                       'officer_email' => $this->input->post('customer_email'),
                       'officer_password' => $this->hash_password($password),
                      // 'order_custid' => $clientid,
                       
                        );
             $this->load->database();

             $this->load->model('Adminmodel');
             $this->Adminmodel->add_officer($data);
              $data['disbursecount'] = $this->Adminmodel->disburse_count();
             $data['requestcount'] = $this->Adminmodel-> request_count();
             $data['message'] = 'Loan officer added Successfully';

            $this->load->view('adminaddofficer', $data);
           }
           else
           {
               $this->load->view('admin_login');
           }
             
        }      
       public function receive_money_process()
	{

             
              
              
              date_default_timezone_set('Africa/Nairobi');
              $now = date('Y-m-d H:i:s');
             
              $data= array(
                 
                       
                                           
                       'received_phone' => $this->input->post('customer_phone'),
                       'received_amount' => $this->input->post('amount'),
                       'received_transaction_id' => $this->input->post('mpesa_code'),
                       'received_date' => $now,
                     
                       
                        );
             $this->load->database();

             $this->load->model('Adminmodel');
             $this->Adminmodel->receive_money($data);
             
             //check if loan is complete and effect changes
             //get total received
             $this->load->database();

             $this->load->model('Adminmodel');
             $phone      = $this->input->post('customer_phone');
             $totalreceived=$this->Adminmodel->gettotalreceived($phone);
             
             //echo $totalreceived
             
             //get total expected pay
             $this->load->model('Adminmodel');
            
             $totalexpecte=$this->Adminmodel->gettotalexpected($phone);
             $totalexpected= $totalexpecte*1.2;
             $data['disbursecount'] = $this->Adminmodel->disburse_count();
             $data['requestcount'] = $this->Adminmodel-> request_count();
             $data['message'] = 'Customer records updated successfully';
             //compare and check
             if($totalreceived>=$totalexpected)
             {
                 //change status of customer
                 $this->load->database();

                 $this->load->model('Adminmodel');
            
                 $this->Adminmodel->updatecustomer($phone);
                 
                 
                 //change statu of loan request
                 
                 $this->load->database();

                 $this->load->model('Adminmodel');
                 
                 //get max id 
            
                 $id=$this->Adminmodel->get_max_id_complete($phone);
                 //update
                  $this->Adminmodel->updateloanrequest($id);
                 
             }
            $this->load->view('payment_success', $data);
             
        }      
}

?>