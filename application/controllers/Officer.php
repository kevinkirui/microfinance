<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function __construct()
	{
		
		$this->load->helper('url');
                
             
                //$this->load->library("pagination");

                $this->load->database();

                // Load form helper library
                //$this->load->library('cart');

                // Load form validation library
                $this->load->library('form_validation');

                // Load session library
                $this->load->library('session');

                // Load database
                $this->load->model('Officermodel');
                
                $this->load->helper('form');
                
                $this->load->helper('date');


                

	}

class Officer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
         public function index()
	{

            $this->load->view('officer_login');
             
        }
        
         public function header()
	{

            $this->load->view('officer_header');
             
        }
        
         public function login()
	{

            $this->load->view('officer_login');
             
        }
        
      public function change_view()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           //$data['h'] = $this->Adminmodel->get_officers();
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('officer_change',$data);
           }
           else
           {
               $this->load->view('officer_change');
           }
             
        }
        
         private function hash_password($password){
             return password_hash($password, PASSWORD_BCRYPT);
        }
        
         public function change_process()
	{
	    
	     $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            $email= $this->session->userdata['logged_in']['client_email'];
            //get the old password and check if it is correct
            $old_pass        = $this->input->post('old');
             $password        = $this->input->post('password');
            $pao=$this->hash_password($password);

           $this->load->database();
     
           $this->load->model('Adminmodel');
           

           $result = $this->Adminmodel->confamclient($old_pass,$email);
           
           if($result===true)
           {
                $this->load->library('form_validation');
               $this->form_validation->set_rules('password', 'Password', 'required');
               $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
               
               

                if ($this->form_validation->run() == FALSE)
                {
                        $data['disbursecount'] = $this->Adminmodel->disburse_count();
                        $data['requestcount'] = $this->Adminmodel-> request_count();
                        $this->load->view('officer_change',$data);
                }
                else
                {
                        //update password
                        $dasa=array(
                            
                               'officer_password'=>$pao,
                            );
                         $data['messo'] = "password update successfully";
                        $this->Adminmodel->update_pass_client($email,$dasa);
                        $data['disbursecount'] = $this->Adminmodel->disburse_count();
                        $data['requestcount'] = $this->Adminmodel-> request_count();
                        $this->load->view('officer_change',$data);
                }
               
               
           }
           
           else
           {
               
           $data['message']="Your old password is wrong";    
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            $this->load->view('officer_change',$data);
               
           }
           
           //confirm new passwords
           
          
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
        
        public function add_client()
	{
            $this->load->database();
             $this->load->library('session');
              
           if($this->session->userdata('logged_in')){

           
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

            $this->load->model('Officermodel');
            $data['level']=$this->Officermodel->level();
            $this->load->view('officeraddcust',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
         public function truestatus()
	{
           $this->load->library('session');
           if($this->session->userdata('logged_in')){
               
           $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           
           //add date params
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['from'] = $from;
           $data['to'] = $to;
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('officer_true_status',$data);
 
   
            }else{
                 $this->load->view('officer_login');
            }
                       
        }
        
         public function update()
	{
           // $status =  $this->uri->segment(3);
            $idnumber =  $this->uri->segment(4);
            
            $this->load->database();

            $this->load->model('Officermodel');

            $this->Officermodel->update($idnumber);
               
            $this->my_clients();
             
        }
         
        public function my_clients()
	{

             $this->load->library('session');
             if($this->session->userdata('logged_in')){

            
             $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
           
            $this->session->userdata['logged_in'];
            
            $client_email = ($this->session->userdata['logged_in']['client_email']);
            $cust_name = ($this->session->userdata['logged_in']['client_name']);
            $this->load->database();

            $this->load->model('Officermodel');
            $data['h']=$this->Officermodel->my_clients($client_email);
            $data['level']=$this->Officermodel->get_products();
            

            $this->load->view('my_clients', $data);
             }
            else
            {
                 $this->load->view('officer_login'); 
            }
             
        }
        
         public function clients_active()
	{

             $this->load->library('session');
            if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

            
           
            $this->session->userdata['logged_in'];
            
            $client_email = ($this->session->userdata['logged_in']['client_email']);
            $cust_name = ($this->session->userdata['logged_in']['client_name']);
            $this->load->database();

            $this->load->model('Officermodel');
            $data['h']=$this->Officermodel->clients_active($client_email);
            

            $this->load->view('officer_active', $data);
            }
            else
            {
                 $this->load->view('officer_login'); 
            }
             
        }
        
        public function clients_rejected()
	{

             $this->load->library('session');
            if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           
            $this->session->userdata['logged_in'];
            
            $client_email = ($this->session->userdata['logged_in']['client_email']);
            $cust_name = ($this->session->userdata['logged_in']['client_name']);
            $this->load->database();

            $this->load->model('Officermodel');
            $data['h']=$this->Officermodel->clients_rejected($client_email);
            

            $this->load->view('officer_rejected', $data);
            }
            else
            {
                 $this->load->view('officer_login'); 
            }
             
        }
        
        public function logout() 
       {

// Removing session data
        $sess_array = array(
        'client_name' => ''
        );
         $this->load->library('session');
        $this->session->unset_userdata('logged_in', $sess_array);
        $data['message_display'] = 'Successfully Logout';
        $this->load->view('officer_login', $data);
     }
         public function all_customers()
	{
           $this->load->library('session');
            if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_customers();
           
            $this->load->view('officer_allcustomers',$data);
            }
            else
            {
                $this->load->view('officer_login');
            }
        }
         
        public function add_client_process()
	{
            //check if user is logged in
            $this->load->library('session');
            if($this->session->userdata('logged_in')){
                
                $this->load->helper('form');

                $this->load->library('form_validation');
                if(file_exists($_FILES['userfile']['tmp_name']))
                    {
                    //start of upload class
                    $config['upload_path']          = './uploads/';
                    $config['allowed_types']        = 'gif|jpg|png|pdf|doc';
                    $config['max_size']             = 10000;
                    $config['max_width']            = 10000;
                    $config['max_height']           = 10000;
    
                    $this->load->library('upload', $config);
    
                    if ( ! $this->upload->do_upload('userfile'))
                    {
                            $error = array('error' => $this->upload->display_errors());
    
                            $this->load->view('officeraddcust', $error);
                    }
                    else
                    {
                                   

                $this->form_validation->set_rules('customer_fname', 'text', 'required', array('required' => 'Customer first name is required'));
                $this->form_validation->set_rules('customer_mname', 'text', 'required', array('required' => 'Customer middle name is required'));
                $this->form_validation->set_rules('customer_lname', 'text', 'required', array('required' => 'Customer last name is required'));
               // $this->form_validation->set_rules('customer_id_number', 'customer_id_number', 'is_unique[customer.customer_id_number]',array('is_unique'=>'Customer with the ID number already exists'));
               // $this->form_validation->set_rules('customer_phone', 'customer_phone', 'is_unique[customer.customer_phone]',array('is_unique' => 'Customer with the entered phone number already exists'));
                $this->form_validation->set_rules('customer_occupation', 'text', 'required', array('required' => 'Customer Occupation is required'));

               
                $this->form_validation->set_rules('customer_email', 'Email');

                if ($this->form_validation->run() == FALSE)
                {
                        $this->load->database();

                        $this->load->model('Officermodel');
                        $data['level']=$this->Officermodel->level();
                        $this->load->view('officeraddcust',$data);
                }
                else
                {
                
                            //Get uploaded class file name
                            $upload_data = $this->upload->data(); 
                            $file_name =   $upload_data['file_name'];
                                
                             $data['jina'] = $this->session->userdata['logged_in']['client_name'];
                           
                            $this->session->userdata['logged_in'];
                            
                            $client_email = ($this->session->userdata['logged_in']['client_email']);
                            $cust_name = ($this->session->userdata['logged_in']['client_name']);
                            
                            //add now time
                             date_default_timezone_set('Africa/Nairobi');
                             $now = date('Y-m-d H:i:s');
                                 
                             $data= array(
                                 
                                       
                                                           
                                       'customer_fname' => $this->input->post('customer_fname'),
                                        'customer_middlename' => $this->input->post('customer_mname'),
                                       'customer_lname' => $this->input->post('customer_lname'),
                                       'customer_id_number' => $this->input->post('customer_id_number'),
                                       'customer_phone' => $this->input->post('customer_phone'),
                                       'customer_email' => $this->input->post('customer_email'),
                                       'location_id' => $this->input->post('location'),
                                       'loan_status' => 1,
                                       'loan_officer_email' => $client_email  ,
                                       'customer_occupation' => $this->input->post('customer_occupation'),
                                       'customer_document' => $file_name,
                                        'added_date' => $now,
                                      // 'order_custid' => $clientid,
                                       
                                        );
                             $gdata= array(
                                 
                                       
                                                           
                                       'guarantor_fname' => $this->input->post('guarantor_fname'),
                                        'guarantor_mname' => $this->input->post('guarantor_mname'),
                                       'guarantor_lname' => $this->input->post('guarantor_lname'),
                                       'guarantor_id_number' => $this->input->post('guarantor_id_number'),
                                       'guarantor_phone' => $this->input->post('guarantor_phone'),
                                       'guarantor_krapin' => $this->input->post('guarantor_krapin'),
                                       'customer_phone' => $this->input->post('customer_phone'),
                                       
                                       
                                       'guarantor_occupation' => $this->input->post('guarantor_occupation'),
                                        'date_created' => $now,
                                      // 'order_custid' => $clientid,
                                       
                                        );
                             //add client
                             $this->load->database();
                
                             $this->load->model('Officermodel');
                             $this->Officermodel->add_client($data);
                             
                             //add guarantor
                             $this->load->database();
                
                             $this->load->model('Officermodel');
                             $this->Officermodel->add_guarantor($gdata);
                             
                             $data['level']=$this->Officermodel->level();
                             $data['message'] = 'Client added Successfully';
                
                            $this->load->view('officer_success', $data);
                                }
                            
                    }
                } 
                
                //end of upload class
                else
                {
               //  $this->load->library('form_validation');

               // $this->form_validation->set_rules('customer_fname', 'text', 'required', array('required' => 'Customer first name is required'));
               // $this->form_validation->set_rules('customer_mname', 'text', 'required', array('required' => 'Customer middle name is required'));
               // $this->form_validation->set_rules('customer_lname', 'text', 'required', array('required' => 'Customer last name is required'));
               // $this->form_validation->set_rules('customer_id_number', 'customer_id_number', 'is_unique[customer.customer_id_number]',array('is_unique'=>'Customer with the ID number already exists'));
               // $this->form_validation->set_rules('customer_phone', 'customer_phone', 'is_unique[customer.customer_phone]',array('is_unique' => 'Customer with the entered phone number already exists'));
               // $this->form_validation->set_rules('customer_occupation', 'text', 'required', array('required' => 'Customer Occupation is required'));

               
              //  $this->form_validation->set_rules('customer_email', 'Email');
             /*
                if ($this->form_validation->run() == FALSE)
                {
                        $this->load->database();

                        $this->load->model('Officermodel');
                        $data['level']=$this->Officermodel->level();
                        $this->load->view('officeraddcust',$data);
                }
                else
                {
                
                */
                
             $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           
            $this->session->userdata['logged_in'];
            
            $client_email = ($this->session->userdata['logged_in']['client_email']);
            $cust_name = ($this->session->userdata['logged_in']['client_name']);
            
            //add now time
             date_default_timezone_set('Africa/Nairobi');
             $now = date('Y-m-d H:i:s');
                 
             $data= array(
                 
                       
                                           
                       'customer_fname' => $this->input->post('customer_fname'),
                        'customer_middlename' => $this->input->post('customer_mname'),
                       'customer_lname' => $this->input->post('customer_lname'),
                       'customer_id_number' => $this->input->post('customer_id_number'),
                       'customer_phone' => $this->input->post('customer_phone'),
                       'customer_email' => $this->input->post('customer_email'),
                       'location_id' => $this->input->post('location'),
                       'loan_status' => 1,
                       'loan_officer_email' => $client_email  ,
                       'customer_occupation' => $this->input->post('customer_occupation'),
                        'added_date' => $now,
                      // 'order_custid' => $clientid,
                       
                        );
             $gdata= array(
                 
                       
                                           
                       'guarantor_fname' => $this->input->post('guarantor_fname'),
                        'guarantor_mname' => $this->input->post('guarantor_mname'),
                       'guarantor_lname' => $this->input->post('guarantor_lname'),
                       'guarantor_id_number' => $this->input->post('guarantor_id_number'),
                       'guarantor_phone' => $this->input->post('guarantor_phone'),
                       'guarantor_krapin' => $this->input->post('guarantor_krapin'),
                       'customer_phone' => $this->input->post('customer_phone'),
                       
                       
                       'guarantor_occupation' => $this->input->post('guarantor_occupation'),
                        'date_created' => $now,
                      // 'order_custid' => $clientid,
                       
                        );
             //add client
             $this->load->database();

             $this->load->model('Officermodel');
             $this->Officermodel->add_client($data);
             
             //add guarantor
             $this->load->database();

             $this->load->model('Officermodel');
             $this->Officermodel->add_guarantor($gdata);
             
             $data['level']=$this->Officermodel->level();
             $data['message'] = 'Client added Successfully';

            $this->load->view('officer_success', $data);
                }
            }
           
            else
            {
                 $this->load->view('officer_login'); 
                
            }
             
        }
        
         public function activeloans()
	{
           $this->load->library('session');
           if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
            //add filter session
           $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['from']=$from;
           $data['to']=$to;
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
          
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           
            $this->load->view('officer_act',$data);
           }
           else
           {
                $this->load->view('officer_login'); 
           }
             
        }
        
        public function view_statement()
	{
           //display reason form and pass the variables
             $this->load->library('session');
           if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
           $phone=  $this->uri->segment(3);
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                 $data['phone']=$phone;
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('officer_mpesa_repayments',$data);
               
           }
          
           else
           {
               
           
           $this->load->view('officer_mpesa_repayments',$data);
           
           
           }
           }
           else
           {
                 $this->load->view('officer_login');
           }
             
        }
        
         public function view_bankstatement()
	{
	     $this->load->library('session');
           if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           //display reason form and pass the variables
           $phone=  $this->uri->segment(3);
           
           $this->load->database();
     
           if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
             
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
            $data['phone'] = $phone;

           $result=$this->Adminmodel->mpesa_statement($phone,$from,$to);
           
           if($result!=FALSE)
           {
                $data['h']=$this->Adminmodel->mpesa_statement($phone,$from,$to);
                $this->load->view('officer_mpesa_statement',$data);
               
           }
          
           else
           {
               
           
           $this->load->view('officer_mpesa_repayments',$data);
           
           
           }
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
          public function paymentschedule()
	{
	       //add sessions
	        $this->load->library('session');
           if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           //display reason form and pass the variables
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h']=$this->Adminmodel->paymentschedule($phone);
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           date_default_timezone_set('Africa/Nairobi');
           $now = date('Y-m-d H:i:s');
            $data['leo'] = $now;
         
            
           
           $this->load->view('officer_payment_schedule',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
        public function guarantor_details()
	{
	       //add session data
	        $this->load->library('session');
           if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           //display reason form and pass the variables
           $phone =  $this->uri->segment(3);
           //$data['id']=$idnumber;
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $data['h']=$this->Adminmodel->guarantor_details($phone);
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           date_default_timezone_set('Africa/Nairobi');
           $now = date('Y-m-d H:i:s');
           $data['leo'] = $now;
         
            
           
           $this->load->view('guarantor_details',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
        }
        public function loan_arrears()
	{
           $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           $this->load->database();
           
           
     
           $this->load->model('Adminmodel');
           
           $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['from'] = $from;
             $data['to'] = $to;
           $data['h'] = $this->Adminmodel->truestatus($from,$to);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('officer_arrears',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
        }
        
        public function my_arrears()
	{
           $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
             $client_email = ($this->session->userdata['logged_in']['client_email']);

           $this->load->database();
           
           
     
           $this->load->model('Adminmodel');
           
           $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');
            $data['from'] = $from;
             $data['to'] = $to;
           $data['h'] = $this->Adminmodel->truestatus_my($from,$to,$client_email);
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           $data['leo'] = $now;
          //  $data['g'] = $this->Adminmodel->sum_received();
           
            $this->load->view('my_arrears',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
        }
        
        public function all_disbursed()
	{
            $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['from'] = $from;
           $data['to'] = $to;
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           //count
            $data['kimumucount'] = $this->Adminmodel->kimumucount($from,$to);
             $data['regionthree'] = $this->Adminmodel->regionthreecount($from,$to);
             $data['mailicount'] = $this->Adminmodel->mailicount($from,$to);
          //sum
             $data['mailisum'] = $this->Adminmodel->mailisum($from,$to);
             $data['regionthreesum'] = $this->Adminmodel->regionthreesum($from,$to);
              $data['kimumusum'] = $this->Adminmodel->kimumusum($from,$to);

           $data['kimumu'] = $this->Adminmodel->all_disbursed_kimumu($from,$to);
           $data['maili'] = $this->Adminmodel->all_disbursed_maili($from,$to);
            $data['three'] = $this->Adminmodel->all_disbursed_three($from,$to);
           $this->load->view('officer_disbursed',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
        public function rejected()
	{
             $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
           $this->load->database();
     
           $this->load->model('Adminmodel');

           $result = $this->Adminmodel->rejected();
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {
              $data['h'] = $this->Adminmodel->rejected();
           
              $this->load->view('officer_rej',$data);
            
           }
           else
           {
                 $data['message'] = "Sorry! There are no records at the moment";
                
                 $this->load->view('officer_rejected',$data);

               
           }
         }
         else
         {
             $this->load->view('officer_login');
         }
             
        }
        
         public function mpesa_repayments()
	{
           $this->load->library('session');
            if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_customers();
           
            $this->load->view('officer_repayments_view',$data);
            }
            else
            {
                $this->load->view('officer_login');
            }
        }
        
         public function all_loans()
	{
           $this->load->library('session');
           if($this->session->userdata('logged_in')){

            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
            
             
              $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
               

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();

           $data['h'] = $this->Adminmodel->all_loans($from,$to);
           
            $this->load->view('officer_allloans',$data);
           }
           else
           {
               $this->load->view('officer_login');
           }
             
        }
        
         
        public function pending_approval()
	{
           $this->load->library('session');
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];

           $this->load->database();
     
           $this->load->model('Adminmodel');

           $result = $this->Adminmodel->display_approve();
           
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {
           
               $data['h'] = $this->Adminmodel->display_approve();

               $this->load->view('officer_approve_pending',$data);
               
           }
           else
           {
                 $data['message'] = "Sorry! There are no records at the moment";
                
                 $this->load->view('officer_approve_pending',$data);
               
           }
           
          
             
        }
        
          public function awaiting_disbursement()
	{
           $this->load->library('session');
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
           $this->load->database();
     
           $this->load->model('Adminmodel');
           $result= $this->Adminmodel->display_awaiting_disburse();
            $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           if($result!=FALSE)
           {


           $data['h'] = $this->Adminmodel->display_awaiting_disburse();
           
           $this->load->view('officer_awaiting_disbursement',$data);
           
           }
           else
           {
               $data['message'] = "Sorry! There are no records at the moment";
                
               $this->load->view('officer_awaiting_disbursement',$data);
               
           }
             
        }
        
         public function loans_duetoday()
	{
           $this->load->library('session');
            if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
            //get date filter values
            $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
             

           $this->load->database();
     
           $this->load->model('Adminmodel');
           
           $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
            date_default_timezone_set('Africa/Nairobi');
            $now = date('Y-m-d H:i:s');

           $data['h'] = $this->Adminmodel->truestatus($from,$to);
           $data['k'] = $this->Adminmodel->truestatuskev();
          //  $data['g'] = $this->Adminmodel->sum_received();
            $data['leo'] =  $now;
            $this->load->view('officer_duetoday',$data);
            }
            else
            {
                $this->load->view('officer_login'); 
            }
        }
        
        
         public function admin_balance()
	{
            $this->load->library('session');
            if($this->session->userdata('logged_in')){
            $data['jina'] = $this->session->userdata['logged_in']['client_name'];
            
             $fro= $this->input->post('from');
              $t= $this->input->post('to');
              $from = date("Y-m-d 00:00", strtotime($fro));
              $to = date("Y-m-d 23:59", strtotime($t));
              
            
             if(($this->input->post('from')) && ($this->input->post('to') ))
             {
                $from=  $from;
                $to=  $to;
             }
             else
             {
                 $from="2015-01-01 00:00:00";
                 $to="2022-01-01 00:00:00";
                
                 
             }
            
           
           $this->load->database();
     
           $this->load->model('Adminmodel');
           

           $data['from'] = $from;
           $data['to'] = $to;
          $data['disbursecount'] = $this->Adminmodel->disburse_count();
           $data['requestcount'] = $this->Adminmodel-> request_count();
           
           //count
           $data['regionthree'] = $this->Adminmodel->regtatubalcount($from,$to);
           $data['kimumucount'] = $this->Adminmodel->kimumtolscount($from,$to);
            $data['mailicount'] = $this->Adminmodel->mailitolscount($from,$to);
           
           
           //get total amounts received 
           $data['regtatuz'] = $this->Adminmodel->regtatubal($from,$to);
           $data['kimumtols'] = $this->Adminmodel->kimumtols($from,$to);
            $data['mailitols'] = $this->Adminmodel->mailitols($from,$to);
           
           //get individual records
           $data['regtatu'] = $this->Adminmodel->regthreebal($from,$to);
           $data['kimbal'] = $this->Adminmodel->kimumubal($from,$to);
           $data['mailbal'] = $this->Adminmodel->mailibal($from,$to);
           
           
           
           $this->load->view('officer_balance',$data);
           
            }
            else
            {
                $this->load->view('officer_login'); 
            }
             
        }
        
        
        public function request_loan()
        {
          date_default_timezone_set('Africa/Nairobi');
          $now = date('Y-m-d H:i:s');
          //unique reference number
          $bytes=5;
          $rand = mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM);
          $refno=bin2hex($rand);
          
          
          $data= array(
                 
                       
                                           
                       'customer_id' => $this->input->post('customer_id'),
                       'mpesa_phone' => $this->input->post('customer_phone'),
                       'request_amount' => $this->input->post('amount'),
                       'product_id' => $this->input->post('product'),

                       'requested_date' => $now,
                       'ref_no' => $refno,

                      // 'order_custid' => $clientid,
                       
                        );
             $this->load->database();

             $this->load->model('Officermodel');
             $this->Officermodel->add_request($data);  
             
            //change status
            $phone = $this->input->post('customer_phone');
             
            $this->load->database();

            $this->load->model('Officermodel');

            $this->Officermodel->update($phone);
               
             redirect('officer/my_clients');
            
            
        }
        
         public function user_login_process()
        {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('officer_email', 'Email', 'trim|required');
            $this->form_validation->set_rules('officer_password', 'Password', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

                if ($this->form_validation->run() == FALSE) 
                  {

                     if(isset($this->session->userdata['logged_in']))

                       {

                            $this->my_clients();

                       }
                   else
                    {

                           $this->load->view('officer_login');

                    }
                 } 
              else 
                 {
                      $data = array(
                      'client_email' => $this->input->post('officer_email'),
                      'client_password' => $this->input->post('officer_password')
              );

               $this->load->database();

               $this->load->model('Officermodel');

               $result = $this->Officermodel->login($data);

        if ($result == TRUE) 

          {

            $client_email = $this->input->post('officer_email');
            $result = $this->Officermodel->read_user_information($client_email);
            if ($result != false) 
               {
                $session_data = array(
                'client_name' => $result[0]->officer_fname,
                'client_email' => $result[0]->officer_email,
                 );
            // Add user data in session
             $this->load->library('session');

                $this->session->set_userdata('logged_in', $session_data);
                        // $this->load->database();

                     //  $this->load->model('Clientmodel');

                     // $data['check'] = $this->Clientmodel->get_myorders($client_email);

                            $this->my_clients();
            }
         } 
        else 
        {

        $data['error_message'] ='Invalid Username or Password';
        $this->load->view('officer_login', $data);
            }
        }
}
        
         
}
?>